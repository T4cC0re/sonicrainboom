# SonicRainBoom

This repo is primarily hosted on [gitlab.com](https://gitlab.com/T4cC0re/sonicrainboom). Please go there to raise issues or contribute.

## Building

This application is built and deployed with [GitLab AutoDevOps](https://about.gitlab.com/stages-devops-lifecycle/auto-devops/) with some [customizations](https://docs.gitlab.com/ee/topics/autodevops/customize.html#customizing-gitlab-ciyml).

You can however also build and run locally. To end up with an instance running on `http://localhost` just run:

```
cp docker-compose.yml.example docker-compose.yml
docker-compose up -d
```
