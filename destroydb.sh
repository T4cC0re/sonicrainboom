#!/usr/bin/env bash

mysql $@ <<EOF
SELECT GROUP_CONCAT(table_schema, '.', table_name) INTO @tables FROM information_schema.tables WHERE table_schema ='sonicrainboom';
SET @tables = CONCAT('DROP TABLE ', @tables);
PREPARE stmt FROM @tables;
SET FOREIGN_KEY_CHECKS = 0;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
SET FOREIGN_KEY_CHECKS = 1;
EOF