package storage_backends

import (
	"encoding/hex"
	"fmt"
	"gitlab.com/T4cC0re/time-track"
	"gitlab.com/sonicrainboom/sonicrainboom/storage_backends/types"
	"golang.org/x/crypto/sha3"
	"io"
	"os"
	"path/filepath"
	"strings"
	"syscall"
	"time"
)

func init() {

	cwd, _ := os.Getwd()
	basePath := fmt.Sprintf("%s/media", cwd)
	err := os.MkdirAll(basePath, os.ModePerm)
	if err != nil {
		log.Fatal(err)
	}
	fs := Filesystem{basePath: basePath}
	AddBackend("local", fs)
	AddSchema("local", "localmedia")
}

type Filesystem struct {
	basePath string
}

func (t Filesystem) Traverse(folder string, blacklist []string, returnChannel chan string) error {
	err := filepath.Walk(folder, func(path string, f os.FileInfo, err error) error {
		if f != nil {
			if f.IsDir() {
				for _, blacklisted := range blacklist {
					if f.Name() == blacklisted || strings.Contains(path, blacklisted) {
						return filepath.SkipDir
					}
				}
			}

			returnChannel <- "file://" + path
		}
		return nil
	})

	return err
}

func copy(a string, b string) error {
	defer timetrack.TimeTrack(time.Now())
	srcFile, err := os.Open(a)
	if err != nil {
		return err
	}
	defer srcFile.Close()

	destFile, err := os.Create(b) // creates if file doesn't exist

	if err != nil {
		return err
	}
	defer destFile.Close()

	_, err = io.Copy(destFile, srcFile) // check first var for number of bytes copied

	if err != nil {
		return err
	}
	err = destFile.Sync()

	if err != nil {
		return err
	}
	return nil
}

func (t Filesystem) Save(path string) (accessor string, err error) {
	defer timetrack.TimeTrack(time.Now())
	f, err := os.Open(path)
	if err != nil {
		return "", nil
	}
	defer f.Close()

	hasher := sha3.New256()

	if _, err := io.Copy(hasher, f); err != nil {
		return "", nil
	}
	hash := hex.EncodeToString(hasher.Sum(nil))
	newFolder := fmt.Sprintf("%s/%s", t.basePath, hash[:2])
	newName := newFolder + "/" + hash[2:]
	err = os.MkdirAll(newFolder, os.ModePerm)
	if err != nil {
		return "", err
	}

	err = os.Rename(path, newName)
	if err != nil {
		err = copy(path, newName)
		if err != nil {
			return "", err
		}
		err = syscall.Unlink(path)
		if err != nil {
			return "", err
		}
	}

	return "localmedia://" + hash, nil
}

func (t Filesystem) Retrieve(accessor string) (uri string, perm types.PermissionSet, err error) {
	defer timetrack.TimeTrack(time.Now())
	splits := strings.Split(accessor, ":")
	if splits[0] != "localmedia" {
		return "", types.PermAll, types.EUnknownScheme
	}

	hash := strings.Replace(splits[1], "/", "", -1)
	filename := fmt.Sprintf("%s/%s/%s", t.basePath, hash[:2], hash[2:])

	if _, err := os.Stat(filename); err != nil {
		return "", types.PermissionSet{},
			err
	}

	return "file://" + filename, types.PermAll, nil
}
