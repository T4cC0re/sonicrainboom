package types

import (
	"errors"
	"fmt"
)

type PermissionSet struct {
	All         bool
	Stream      bool
	Cache       bool
	DeviceCache bool
	Convert     bool
	Import      bool
}

type StorageBackend interface {
	Save(path string) (accessor string, err error)
	Retrieve(accessor string) (string, PermissionSet, error)
	Traverse(folder string, blacklist []string, returnChannel chan string) error
}

type PermissionError struct {
	errorText string
}

var ENoBackend = errors.New("no storage backend available")
var EUnknownScheme = errors.New("schema unknown to backend")
var ENotFoundOrEmpty = errors.New("file not found or empty")
var EUnsupported = errors.New("unsupported action")

var PermAll = PermissionSet{All: true}

func (e PermissionError) Error() string {
	return fmt.Sprintf("PermissionError: %s", e.errorText)
}

func (ps *PermissionSet) EnsureStream() error {
	return Ensure(ps.All || ps.Stream, "content may not be streamed")
}

func (ps *PermissionSet) EnsureCache() error {
	return Ensure(ps.All || ps.Cache, "content may not be cached")
}

func (ps *PermissionSet) EnsureDeviceCache() error {
	return Ensure(ps.All || ps.DeviceCache, "content may not be cached on device")
}

func (ps *PermissionSet) EnsureConvert() error {
	return Ensure(ps.All || ps.Convert, "content may not be converted")
}

func (ps *PermissionSet) EnsureImport() error {
	return Ensure(ps.All || ps.Import, "content may not be imported")
}

func Ensure(condition bool, errorText string) error {
	if !condition {
		return PermissionError{errorText: errorText}
	}
	return nil
}
