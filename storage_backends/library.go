package storage_backends

import (
	"errors"
	"fmt"
	sqlx_import "github.com/jmoiron/sqlx"
	"gitlab.com/T4cC0re/time-track"
	"gitlab.com/sonicrainboom/sonicrainboom/storage_backends/types"
	"os"
	"path"
	"strconv"
	"strings"
	"time"
)

const schema = "library"

var ENoLibrary = errors.New("no library available")

/**
Has to be shared across instances because of weirdness...
*/
var sqlx *sqlx_import.DB

func init() {
	log.Debug("Initializing library...")
	lib := Library{}
	AddBackend("lib", lib)
	AddSchema("lib", schema)
	log.Debug("Initializing library Done...")
}

type Library struct {
}

func pathOk(path string) bool {
	info, err := os.Stat(path)
	if err != nil || info.Size() <= 0 || info.IsDir() == true {
		log.WithField("path", path).Warn("not found")
		return false
	}
	return true
}

func (t Library) SetDB(newdb *sqlx_import.DB) {
	log.Infof("%+v", newdb)
	sqlx = newdb
	log.Infof("%+v", sqlx)

	for id, libPath := range t.GetAllLibraries() {
		log.WithField("path", libPath).WithField("id", id).Infoln("found library!")
		// TODO: inotify maybe?
	}
}

func (t Library) GetLibrary(id int) (path string, err error) {
	defer timetrack.TimeTrack(time.Now())

	//type lib struct {
	//	Id   int
	//	Path string
	//}
	//var libe lib

	err = sqlx.Get(&path, sqlx.Rebind("SELECT path FROM libraries WHERE id = ?"), id)
	if err != nil {
		log.Errorf("querying library list failed. %s", err.Error())
		return
	}

	if path == "" {
		err = ENoLibrary
	}

	//return libe.Path, err

	return
}

func (t Library) GetAllLibraries() (libs map[int]string) {
	defer timetrack.TimeTrack(time.Now())
	libs = make(map[int]string)

	type dbLib struct {
		Id   int
		Path string
	}
	var libraries []dbLib

	err := sqlx.Select(&libraries, "SELECT id, path FROM libraries")
	if err != nil {
		log.Errorf("querying library list failed. %s", err.Error())
		return
	}

	for _, lib := range libraries {
		libs[lib.Id] = lib.Path
	}

	return
}

func (t Library) Save(filePath string) (accessor string, err error) {
	defer timetrack.TimeTrack(time.Now())
	filePath = path.Clean(filePath)
	if !pathOk(filePath) {
		return "", types.ENotFoundOrEmpty
	}

	var libraryId int
	var libraryPath string

	for id, libPath := range t.GetAllLibraries() {
		// TODO: Optimize DB usage, maybe?
		if !strings.HasPrefix(filePath, libPath) {
			continue
		}
		libraryId = id
		libraryPath = libPath
	}
	if libraryId == 0 {
		return "", ENoLibrary
	}

	return strings.Replace(filePath, libraryPath, fmt.Sprintf("%s://%d/", schema, libraryId), 1), nil
}

func (t Library) Retrieve(accessor string) (uri string, perm types.PermissionSet, err error) {
	defer timetrack.TimeTrack(time.Now())
	if !strings.HasPrefix(accessor, schema+"://") {
		log.Error(accessor)
		return "", types.PermissionSet{}, types.EUnknownScheme
	}

	split := strings.SplitN(accessor, "/", 4)
	log.Infof("%+v", split)
	subPath := strings.Replace(split[3], schema+"://", "", 1)
	libraryId, err := strconv.Atoi(split[2])

	var fullPath string

	prePath, err := t.GetLibrary(libraryId)
	if err != nil {
		return "", types.PermissionSet{}, err
	} else {
		fullPath = path.Clean(prePath + "/" + subPath)
		if !strings.HasPrefix(fullPath, prePath) {
			log.Errorf("Possible exploit: %s", accessor)
			return "", types.PermissionSet{}, types.ENotFoundOrEmpty
		}
	}
	log.Infof("%s", fullPath)

	if !pathOk(fullPath) {
		return "", types.PermissionSet{}, types.ENotFoundOrEmpty
	}

	return "file://" + fullPath, types.PermAll, nil
}

func (t Library) Traverse(folder string, blacklist []string, returnChannel chan string) error {
	defer timetrack.TimeTrack(time.Now())
	return types.EUnsupported
}
