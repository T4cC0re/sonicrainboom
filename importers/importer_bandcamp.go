package importers

import (
	"archive/zip"
	"encoding/json"
	"github.com/docker/distribution/uuid"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"io"
	"io/ioutil"
	"net/http"
	"path"
	"path/filepath"
	"strconv"
	"time"

	//	"net/url"
	"os"
	"regexp"
	"strings"
)

var re = regexp.MustCompile(`(?m)(https?:\/\/[a-zA-Z0-9\.\/]+\?enc=flac.+?)&quot;`)
var bc_errLinkTooFresh = errors.New("unexpected content-type text/html. This may happen if the download link is too fresh. Try again in a few minutes.")
var BC_errExpirationError = errors.New("ExpirationError received. This means the download link has expired. Please open the link in your browser to refresh it.")

func init() {
	AddImporter("Bandcamp", regexp.MustCompile(`(?m)^https://bandcamp\.com/download`), BandcampImport)
}

func BandcampImport(url string, logger logrus.FieldLogger) (err error) {

	var tempDirs []string
	defer func() {
		for _, dir := range tempDirs {
			_ = os.RemoveAll(dir)
		}
	}()

	resp, err := http.Get(url)

	defer resp.Body.Close()
	var html []byte
	html, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	var downloadURLs []string

	// region Extract download URLs from html
	pay_urls := re.FindAllSubmatch(html, -1)
	pay_urls_new := map[string]struct{}{}

	pay_url := ""
	for _, u := range pay_urls {
		pay_url = string(u[1])
		pay_url = strings.Replace(pay_url, "&amp;", "&", -1)
		pay_url = strings.Replace(pay_url, "http://", "https://", -1)

		pay_urls_new[pay_url] = struct{}{}
	}

	for url := range pay_urls_new {
		downloadURLs = append(downloadURLs, url)
	}
	// endregion

	// First prepare everything
	for idx, downloadLink := range downloadURLs {
		retries := 0
	prepare:
		for retries < 3 {
			var newUrl string
			newUrl, err = prepareBandcampDownload(downloadLink, logger)
			if err == nil {
				downloadURLs[idx] = newUrl
				break prepare
			}
			logger.Error(err)
			time.Sleep(10 * time.Second)
			retries++
		}
		if err != nil {
			return
		}
		retries = 0
	}

	// Then download everything
	for _, downloadLink := range downloadURLs {
		retries := 0
	download:
		for retries < 3 {
			var dir string
			dir, err = bandcampDownload(downloadLink, logger)
			tempDirs = append(tempDirs, dir)
			if err == nil {
				break download
			}
			logger.Error(err)
			time.Sleep(10 * time.Second)
			retries++
		}
		if err != nil {
			return
		}
	}

	// Then add files to library
	for _, dir := range tempDirs {
		logger.Infof("Adding '%s' to probe queue...", dir)
		dirinfo, err := ioutil.ReadDir(dir)
		if err != nil {
			return err
		}

		for _, file := range dirinfo {
			logger.Infof("Adding '%s/%s' to probe queue...", dir, file.Name())
			accessor, err := backend.Save(path.Join(dir, file.Name()))
			if err != nil {
				return err
			}
			fn, _, err := backend.Retrieve(accessor)
			logger.Info(fn, err)

			err = library.AddToProbeQueue(accessor)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

type BCPreparedResult struct {
	DownloadURL string `json:"download_url"`
	Result      string `json:"result"`
	Date        string `json:"date"`
	RetryURL    string `json:"retry_url"`
	Host        string `json:"host"`
	URL         string `json:"url"`
	Errortype   string `json:"errortype"`
}

func prepareBandcampDownload(oldUrl string, logger logrus.FieldLogger) (newUrl string, err error) {
	logger.Infof("preparing download of '%s'...", oldUrl)
	url := strings.Replace(oldUrl, "/download", "/statdownload", 1) + "&.rand=1129362132542" + "&.vrs=1"

	var req *http.Request
	req, err = http.NewRequest("GET", url, nil)
	if err != nil {
		return
	}
	req.Header.Add("Accept", "application/json, text/javascript, */*; q=0.01")
	req.Header.Add("Referer", oldUrl)


	var resp *http.Response
	resp, err = http.DefaultClient.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	var bcpr BCPreparedResult
	err = json.Unmarshal(data, &bcpr)
	if err != nil {
		return
	}

	if bcpr.Result != "ok" {
		if bcpr.Errortype == "ExpirationError" {
			return "", BC_errExpirationError
		}
		return "", errors.New(bcpr.Errortype)
	}

	if bcpr.DownloadURL == "" {
		newUrl = oldUrl
	} else {
		newUrl = strings.Replace(bcpr.DownloadURL, "http://", "https://", -1)
	}

	return
}

func bandcampDownload(url string, logger logrus.FieldLogger) (tempdir string, err error) {

	tempdir, err = ioutil.TempDir("", "")
	if err != nil {
		return
	}

	logger.Infof("Starting download of '%s'...", url)
	resp, err := http.Get(url)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return tempdir, errors.New("unexpected response status " + resp.Status)
	}
	if strings.Contains(resp.Header.Get("Content-Type"), "text/html") {
		return tempdir, bc_errLinkTooFresh
	}

	var fileName = path.Join(tempdir, uuid.Generate().String())
	var isArchive bool
	if strings.Contains(resp.Header.Get("Content-Type"), "audio/flac") {
		fileName += ".flac"
		isArchive = false
	} else {
		fileName += ".zip"
		isArchive = true
	}

	var fileLength int64
	fileLength, err = strconv.ParseInt(resp.Header.Get("Content-Length"), 10, 64)
	if err != nil {
		return
	}
	logger.Infof("Request finished with %s. Streaming data to '%s'...", resp.Status, fileName)

	// Create the file
	out, err := os.Create(fileName)
	if err != nil {
		return
	}

	// Write the body to file
	var bytesOut int64
	bytesOut, err = io.Copy(out, resp.Body)
	if err != nil {
		return
	}
	out.Sync()
	out.Close()
	if bytesOut != fileLength {
		err = io.ErrShortWrite
		return
	}

	logger.Infof("Downloading '%s' done", fileName)

	if isArchive {
		logger.Infof("Extracting '%s'...", fileName)
		return tempdir, bandcampUnzip(fileName, tempdir, logger)
	}

	return
}

func bandcampUnzip(src string, tempdir string, logger logrus.FieldLogger) error {

	r, err := zip.OpenReader(src)
	if err != nil {
		return err
	}
	defer r.Close()

	for _, f := range r.File {
		fpath := filepath.Join(tempdir, strings.Replace(f.Name, "/", "", -1))
		logger.Infof("Extracting '%s'...", fpath)

		if f.FileInfo().IsDir() { // No subdirs!
			continue
		}

		outFile, err := os.OpenFile(fpath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
		if err != nil {
			return err
		}

		rc, err := f.Open()
		if err != nil {
			return err
		}

		_, err = io.Copy(outFile, rc)

		outFile.Sync()
		outFile.Close()
		rc.Close()

		if err != nil {
			return err
		}
	}

	return os.Remove(src)
}
