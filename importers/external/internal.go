package external

import (
	importers_types "gitlab.com/sonicrainboom/sonicrainboom/importers/types"

	"regexp"
)

// This is to resolve an import cycle in go

var ImportFuncs = map[string]importers_types.ImportFunc{}
var ImportMatcher = map[string]*regexp.Regexp{}

func RunImport(url string) error {
	for importer, regex := range ImportMatcher {
		if !regex.MatchString(url) {
			continue
		}

		Log.Infof("Found importer '%s' for URL '%s'", importer, url)

		return ImportFuncs[importer](url, Log.WithField("importer", importer))
	}

	Log.Infof("Could not find importer for URL '%s'. %+v", url, ImportFuncs, ImportMatcher)
	return importers_types.ENoImporter
}
