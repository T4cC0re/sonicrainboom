package importers

import (
	importers_types "gitlab.com/sonicrainboom/sonicrainboom/importers/types"
	"gitlab.com/sonicrainboom/sonicrainboom/library_management"
	"gitlab.com/sonicrainboom/sonicrainboom/storage_backends"
	backend_types "gitlab.com/sonicrainboom/sonicrainboom/storage_backends/types"
	"regexp"
	"gitlab.com/sonicrainboom/sonicrainboom/importers/external"
)

var backend backend_types.StorageBackend
var library *library_management.LibraryManagement

func init() {
	var err error
	backend, err = storage_backends.GetBackendByName("local")
	if err != nil {
		external.Log.Fatalln(err)
	}

	library = library_management.Library
}

func AddImporter(name string, urlRegex *regexp.Regexp, importFunc importers_types.ImportFunc) {
	if urlRegex == nil || importFunc == nil {
		return
	}
	external.ImportFuncs[name] = importFunc
	external.ImportMatcher[name] = urlRegex
}
