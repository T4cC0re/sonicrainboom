import {Component, OnInit} from '@angular/core';
import {Player} from "@src/app/player/player";
import {RestService} from "@src/app/rest.service";
import {SonicRainBoom} from "@src/app/sonicrainboom.d";
import SongPopulated = SonicRainBoom.SongPopulated;
import AlbumPopulated = SonicRainBoom.AlbumPopulated;

@Component({
    selector: 'app-player',
    templateUrl: './player.component.html',
    styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit {
    public player: Player;

    private get pausedText(): string {
        return this.player.paused ? "Play" : "Pause"
    }

    constructor(private rest: RestService) {
        if (window.player == null) {
            window.player = new Player(this.rest);
        }
        this.player = window.player;
    }

    ngOnInit() {
    }

    public volumeChanged = (event: Event) => {
        this.player.volume = (event.target as any).value;
    };
    public currentTimeChanged = (event: Event) => {
        this.player.currentTime = (event.target as any).value;
    };

    public playSong = (song: SongPopulated) => this.player.playSong(song);

    public enqueueSong = (song: SongPopulated) => this.player.enqueueSong(song);

    public prepareSong = (song: SongPopulated) => this.player.prepareSong(song);

    public copyLink = (song: SongPopulated) => this.player.copyLink(song);

    public copyConnectLink = () => this.player.copyConnectLink();

    public enqueueAlbum = async (album: AlbumPopulated) => {
        let songs = await this.rest.getSongsByAlbum(album.Id)
        songs.sort(this.rest.songComparator)
        await this.player.enqueueSong(...songs);
    };

    public skip = async () => {
        return this.player.skip();
    };
}
