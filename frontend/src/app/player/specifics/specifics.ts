import {EventEmitter} from "@angular/core";

export class Specifics {
    private audio: HTMLAudioElement;
    private events: EventEmitter<string>

    constructor() {
        this.audio = new Audio();
        this.audio.addEventListener("error", console.log);
        this.audio.crossOrigin = "anonymous";
        this.audio.style.display = "none";
        this.audio.preload = 'auto';
        this.audio.volume = 0.25;
    }

    public preload = async (url: string) => {
        if (!this.audio.paused) {
            this.audio.pause();
        }
        this.audio.src = url;
    };

    public pause = async() => {
        this.audio.pause();
    }

    public tooglePause = async () => {
        if (!this.audio.paused) {
            this.audio.pause();
        } else {
            return this.audio.play();
        }
    };

    public isPaused = (): boolean => {
        return this.audio.paused
    };

    public play = async (url: string) => {
        if (url) {
            await this.preload(url);
        }
        if ('displayNameForTrack' in this.audio) {
            // @ts-ignore
            this.audio.displayNameForTrack()
        }
        await this.audio.play();
    };

    public setVolume = (percent: number) => {
        this.audio.volume = percent / 100;
    };

    public getVolume = (): number => {
        return Math.min(this.audio.volume * 100, 100);
    };

    /**
     * getDuration in seconds
     */
    public getDuration = (): number => {
        return this.audio.duration;
    };

    public getCurrentTime = () => {
        return this.audio.currentTime
    };

    public setCurrentTime = (seconds: number) => {
        this.audio.currentTime = seconds;
    };

    public setEndcallback = (callback: any) => {
        this.audio.onended = callback;
    }
}

export let specifics = new Specifics();
