import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {RestService} from "@src/app/rest.service";
import {SonicRainBoom} from "@src/app/sonicrainboom.d";
import Artist = SonicRainBoom.Artist;
import Album = SonicRainBoom.Album;

type EAlbum = Album & {
    AlbumArtist
}

@Component({
    selector: 'app-artist-detail',
    templateUrl: './artist-detail.component.html',
    styleUrls: ['./artist-detail.component.scss']
})
export class ArtistDetailComponent implements OnInit {
    id: number;
    artist: Artist = {Id: 0, Title: ""};
    albums: EAlbum[] = [];

    constructor(public rest: RestService, private _route: ActivatedRoute) {
    }

    ngOnInit() {
        this.id = parseInt(this._route.snapshot.paramMap.get('id'), 10);
        this.getArtist();
    }

    private getArtist = async () => {
        this.artist = await this.rest.getArtist(this.id);
        this.albums = await this.rest.getArtistAlbums(this.id);
    };
}
