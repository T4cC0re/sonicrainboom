import {Component, Input} from '@angular/core';
import {SonicRainBoom} from "@src/app/sonicrainboom.d";
import Album = SonicRainBoom.Album;

@Component({
    selector: 'app-album-glance',
    templateUrl: './album-glance.component.html',
    styleUrls: ['./album-glance.component.scss']
})
export class AlbumGlanceComponent {
    @Input() album: Album;

    public window: Window;

    constructor() {
        this.window = window;
    }

}
