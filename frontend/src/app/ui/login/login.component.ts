import {Component, Input} from '@angular/core';
import {RestService} from "@src/app/rest.service";
import * as $ from 'jquery';
import {SonicRainBoom} from '@src/app/sonicrainboom';
import OauthProvider = SonicRainBoom.OauthProvider;

@Component({
    selector: 'login',
    templateUrl: './login.component.html'
})
export class LoginComponent {
  public isLoggedIn: boolean = false;
  public areProvidersLoaded: boolean = false;
  public providers: OauthProvider[] = [];

  constructor(private rest: RestService) {
  }

  ngOnInit() {
    this.reload();
  }

  public reload = async () => {
    this.isLoggedIn = await this.rest.isLoggedIn();
    if (this.isLoggedIn) {
      // Skip provider loading if logged in.
      return;
    }
    this.providers = await this.rest.loadProviders();
    if (this.providers.length > 0) {
      this.areProvidersLoaded = true;
    } else {
      this.areProvidersLoaded = false;
    }
  };

  public showLoginModal = async () => {
    $('#loginModal').modal({
      backdrop: false,
    });
  };

  public login = async (provider: OauthProvider) => {
    if (!provider.Endpoint) {
      return;
    }
    document.location.href = `/api/v1/auth/providers/${provider.Endpoint}`
  }

  public logout = async () => {
    document.location.href = `/api/v1/auth/logout`
  }
}
