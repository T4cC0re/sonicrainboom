import {Routes} from '@angular/router';

import {HomeComponent} from '@src/app/home/home.component';
import {ArtistsComponent} from "@src/app/artists/artists.component";
import {ArtistDetailComponent} from "@src/app/artist-detail/artist-detail.component";
import {AlbumDetailComponent} from "@src/app/album-detail/album-detail.component";

export const routes: Routes = [
    {
        path: '',
        // redirectTo: '/album;id=86', //TODO: App debugging only
        pathMatch: 'full',
        component: HomeComponent,
    },
    {
        path: 'album',
        component: AlbumDetailComponent,
        data: {title: 'Album'}
    },
    {
        path: 'artists',
        component: ArtistsComponent,
        data: {title: 'Artists'}
    },
    {
        path: 'artist',
        component: ArtistDetailComponent,
        data: {title: 'Artist'}
    },
];
