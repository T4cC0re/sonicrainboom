import {ChangeDetectorRef, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';

import {AppRoutingModule} from '@src/app/app-routing.module';
import {AppComponent} from '@src/app/app.component';

import {HomeComponent} from '@src/app/home/home.component';
import {ArtistsComponent} from '@src/app/artists/artists.component';
import {ArtistDetailComponent} from '@src/app/artist-detail/artist-detail.component';
import {AlbumGlanceComponent} from '@src/app/ui/album-glance/album-glance.component';
import {AlbumDetailComponent} from '@src/app/album-detail/album-detail.component';
import {PlayerComponent} from '@src/app/player/player.component';
import {LoginComponent} from '@src/app/ui/login/login.component';


@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        ArtistsComponent,
        ArtistDetailComponent,
        AlbumGlanceComponent,
        AlbumDetailComponent,
        PlayerComponent,
        LoginComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
    ],
    providers: [PlayerComponent],
    bootstrap: [AppComponent]
})
export class AppModule {
}
