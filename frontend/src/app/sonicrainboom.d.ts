export module SonicRainBoom {
    type int = number;
    type Artist = {
        Id: number,
        Title: string,
    };

    type Album = {
        Id: int
        Title: string
        AlbumArtistId: int
    };

    type AlbumPopulated = {
        Id: int
        Title: string
        AlbumArtistId: int
        AlbumArtist: Artist
        Format: string
    };

    type Playlist = {
        Id: int
        Title: string
        Owner: int
    }


    type Song = {
        Id: int
        Title: string
        ArtistId: int
        AlbumId: int
        TrackNr: int
        DiscNr: int
        Duration: int
        PlayCount: int
    }

    type SongPopulated = {
        Id: int
        Title: string
        ArtistId: int
        AlbumId: int
        TrackNr: int
        DiscNr: int
        Duration: int
        PlayCount: int
        Artist: Artist
        Format: string
        Album: AlbumPopulated
        Versions: SongVersion[]
        CachedURL?: string
    }

    type SongVersion = {
        Id: number
        SongId: number
        Format: string
        Accessor: string
        Codec: string
        CodecLong: string
        Channels: number
        ChannelLayout: string
        BitRate: number
        BitDepth: number
        SampleRate: number
    }

    type Paginated<T> = {
        LastId: int,
        Data: T,
    }

    type OauthProvider = {
      Name: string
      LogoClass?: string
      Endpoint: string
      Enabled: boolean
    }
}
