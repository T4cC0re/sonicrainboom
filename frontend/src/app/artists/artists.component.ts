import {Component, OnInit} from '@angular/core';
import {RestService} from '../rest.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SonicRainBoom} from "@src/app/sonicrainboom.d";
import Artist = SonicRainBoom.Artist;

@Component({
    selector: 'app-artists',
    templateUrl: './artists.component.html',
    styleUrls: ['./artists.component.scss']
})
export class ArtistsComponent implements OnInit {

    artists: Artist[] = [];
    lastId: number = 0;

    constructor(public rest: RestService, private route: ActivatedRoute, private router: Router) {
    }

    ngOnInit() {
        this.reload()
    }

    private reload = async () => {
        this.lastId = 0;
        this.artists = [];
        await this.getArtists();
    };

    private getArtists = async () => {
        let data = await this.rest.getArtists(this.lastId);
        console.log(data);
        this.lastId = data.LastId || 0;
        if (!data.Data.length) {
            return
        }
        this.rest.titleSortedInsertMultiple(this.artists, ...data.Data);
        await this.getArtists(); // Get the next page
    };

    // add() {
    //   this.router.navigate(['/product-add']);
    // }

    private delete = async (id: number) => {
        try {
            await this.rest.deleteArtist(id);
            this.reload();
        } catch (_) {
        }
    };

}