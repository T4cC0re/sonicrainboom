import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {NativeScriptModule} from 'nativescript-angular/nativescript.module';
import {HttpClientModule} from '@angular/common/http';

import {AppRoutingModule} from '@src/app/app-routing.module';
import {AppComponent} from '@src/app/app.component';
import {HomeComponent} from '@src/app/home/home.component';
import {ArtistsComponent} from '@src/app/artists/artists.component';
import {ArtistDetailComponent} from '@src/app/artist-detail/artist-detail.component';
import {AlbumGlanceComponent} from '@src/app/ui/album-glance/album-glance.component';
import {AlbumDetailComponent} from '@src/app/album-detail/album-detail.component';
import {PlayerComponent} from '@src/app/player/player.component';
import {LoginComponent} from '@src/app/ui/login/login.component';


// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from 'nativescript-angular/forms';

// Uncomment and add to NgModule imports  if you need to use the HTTP wrapper
// import { NativeScriptHttpClientModule } from 'nativescript-angular/http-client';

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        ArtistsComponent,
        ArtistDetailComponent,
        AlbumGlanceComponent,
        AlbumDetailComponent,
        PlayerComponent,
        LoginComponent,
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        HttpClientModule,
    ],
    providers: [PlayerComponent],
    bootstrap: [AppComponent],
    schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {
}
