import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {AppModule} from '@src/app/app.module';
import {environment} from '@src/environments/environment';
import {MessageHub} from "@src/messagehub";
import 'bootstrap';

console.log('%cStop!', 'color: red; font-size: 50px; font-weight: bold;');
console.log('%cThis might not do what you think! Make sure you understand anything you paste or type into here.', 'color: black; font-size: 20px; font-weight: bold;');

if (environment.production) {
    enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
    .catch(err => console.log(err));

window.MessageHub = new MessageHub();

// if ('serviceWorker' in navigator) {
//     navigator.serviceWorker.getRegistrations().then(
//         registrations => {
//             for (let registration of registrations) {
//                 registration.unregister();
//             }
//         }
//     );
// }

if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/ui/webworker.js', {scope: "/"}).then(async (reg) => {
        await reg.update();

        console.log(reg);

        if (reg.installing) {
            console.log('Service worker installing');
        } else if (reg.waiting) {
            console.log('Service worker installed');
        } else if (reg.active) {
            console.log('Service worker active');
        }
    }).catch(function (error) {
        console.log('Registration failed with ' + error);
    });
}
