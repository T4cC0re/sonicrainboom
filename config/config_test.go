package config

import (
	"flag"
	"io/ioutil"
	"os"
	"testing"
)

func init() {
	os.Chdir("..")
}

func BenchmarkGetConfig(b *testing.B) {
	b.StopTimer()
	var cfg Config
	for n := 0; n < b.N; n++ {
		// We need this to silence config flag parsing.
		flag.CommandLine = flag.NewFlagSet("a", flag.ContinueOnError)
		flag.CommandLine.SetOutput(ioutil.Discard)

		b.StartTimer()
		cfg = GetConfig()
		b.StopTimer()
		config = nil
	}
	_ = cfg.WorkDir
}

func BenchmarkCheckCalledFromInit(b *testing.B) {
	for n := 0; n < b.N; n++ {
		checkCalledFromInit()
	}
}
