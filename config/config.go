package config

import (
	"crypto/rand"
	"flag"
	"fmt"
	"github.com/ghodss/yaml"
	srb_log "gitlab.com/sonicrainboom/sonicrainboom/log"
	"io/ioutil"
	"os"
	"path"
	"regexp"
	"runtime"
)

type StorageBackendConfig map[string]interface{}

type Oauth struct {
	GitLabAppID string `json:"gitlab.com,omitempty"`
}

type Config struct {
	StorageBackends map[string]StorageBackendConfig `json:"backends,omitempty"`
	BuiltInSilo     bool                            `json:"silo_builtin,omitempty"`
	SiloURL         string                          `json:"silo_url,omitempty"`
	SiloSecret      string                          `json:"silo_secret,omitempty"`
	SiloPath        string                          `json:"silo_path,omitempty"`
	APIPort         uint16                          `json:"api_port,omitempty"`
	APIPortTLS      uint16                          `json:"api_tls_port,omitempty"`
	Cert            string                          `json:"api_tls_cert,omitempty"`
	Key             string                          `json:"api_tls_key,omitempty"`
	Logfile         string                          `json:"log_file,omitempty"`
	Loglevel        string                          `json:"log_level,omitempty"`
	WorkDir         string                          `json:"workdir,omitempty"`
	DBHost          string                          `json:"db_host,omitempty"`
	DBURL           string                          `json:"db_url,omitempty"`
	JWTSecret       string                          `json:"jwt_secret,omitempty"`
	Oauth           Oauth                           `json:"oauth,omitempty"`
}

var (
	config *Config
)

func GenerateRandomBytes(n int) []byte {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		panic("could not read random data")
	}
	return b
}

func checkCalledFromInit() {
	initRegex := regexp.MustCompile(`(?m)\.init(?:\.\d+)?$`)

	// Skip 2 == The function, that called this one
	pc, _, _, ok := runtime.Caller(2)
	if !ok {
		return
	}
	calledFunction := runtime.FuncForPC(pc)

	// Get all the callers
	callersPC := make([]uintptr, 256)
	runtime.Callers(0, callersPC)
	frames := runtime.CallersFrames(callersPC)

	// While we have frames, check if it is an init() function
	frame, more := frames.Next()
	for more {
		// If it is, fatal sraight outta here.
		if initRegex.MatchString(frame.Function) {
			log.
				WithField("callerSource", frame.Function).
				WithField("callerFile", frame.File).
				WithField("callerLine", frame.Line).
				Fatalf("%s() was called from another init(). This is unsupported.", path.Base(calledFunction.Name()))
		}
		frame, more = frames.Next()
	}
}

func readConfig() {
	checkCalledFromInit()

	var (
		loglevel   = ""
		configfile = "./config.yml"
	)

	// Check if we are running in a test before doing anything.
	if flag.Lookup("test.v") == nil {
		flag.StringVar(&loglevel, "loglevel", loglevel, "Override loglevel to use instead of config value('debug', 'info', 'warn', 'error')")
		flag.StringVar(&configfile, "config", configfile, "configuration file to use")
		flag.Parse()
	}

	if _, err := os.Stat(configfile); os.IsNotExist(err) && configfile != "" {
		log.WithField("config", configfile).WithField("error", err).Warn()
	}

	yml, err := ioutil.ReadFile(configfile)
	if err != nil {
		log.WithField("config", configfile).WithField("error", err).Warn()
	}

	var conf Config
	err = yaml.Unmarshal(yml, &conf)
	if err != nil {
		log.WithField("config", configfile).WithField("error", err).Fatal()
	}

	if conf.Loglevel == "" {
		conf.Loglevel = "info"
	}

	if loglevel != "" {
		conf.Loglevel = loglevel
	}

	err = srb_log.SetLevel(conf.Loglevel)
	if err != nil {
		log.WithField("config", configfile).WithField("error", err).Fatal()
	}

	// Chdir before opening a log file
	if conf.WorkDir != "" {
		err := os.Chdir(conf.WorkDir)
		if err != nil {
			log.WithField("config", configfile).WithField("error", err).Fatal()
		}
	}

	if conf.Logfile == "journal" {
		err = srb_log.EnableJournal()
		if err != nil {
			log.WithField("config", configfile).WithField("error", err).Warn()
		}
	} else if conf.Logfile != "" {
		file, err := os.OpenFile(conf.Logfile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
		if err != nil {
			log.WithField("config", configfile).WithField("error", err).Fatal()
		}
		// Hack Hack
		*os.Stderr = *file
		*os.Stdout = *file
	}

	// We are deployed in GitLabAutoDevOps, force port to 5000
	if val := os.Getenv("GITLAB_ENVIRONMENT_NAME"); val != "" {
		log.Infof("Detected GitLab environment '%s'", val)
		conf.APIPort = 5000
	}

	// Set GitLab.com OAuth AppID
	if val := os.Getenv("OAUTH_GITLABCOM"); val != "" && conf.Oauth.GitLabAppID == "" {
		log.Infof("Set GitLab.com Oauth AppID via environment")
		conf.Oauth.GitLabAppID = val
	}

	if conf.SiloURL == "" {
		conf.BuiltInSilo = true
	}
	if conf.BuiltInSilo {
		conf.SiloURL = ""
	}
	if conf.SiloSecret == "" {
		conf.SiloSecret = fmt.Sprintf("%x", GenerateRandomBytes(16))
	}
	if conf.JWTSecret == "" {
		conf.JWTSecret = fmt.Sprintf("%x", GenerateRandomBytes(16))
	}
	if conf.SiloPath == "" {
		conf.SiloPath = "./cache"
	}

	if conf.DBHost == "" {
		conf.DBHost = "localhost"
	}

	if val := os.Getenv("DATABASE_URL"); val != "" {
		log.Infof("Database URL overridden via 'DATABASE_URL' variable")
		conf.DBURL = val
	}

	if conf.DBURL == "" {
		conf.DBURL = fmt.Sprintf("postgresql://srb_app:sonicrainboom_application@%s/sonicrainboom?sslmode=disable", conf.DBHost)
	}

	config = &conf
}

/*
GetConfig returns a copy of the currently loaded config
*/
func GetConfig() Config {
	if config == nil {
		readConfig()
	}
	return *config
}
