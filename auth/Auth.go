package auth

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/T4cC0re/time-track"
	"gitlab.com/sonicrainboom/sonicrainboom/config"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/gitlab"
	"gopkg.in/square/go-jose.v2"
	"gopkg.in/square/go-jose.v2/jwt"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"
	"time"
)

type Auth struct {
	routeACL map[string][]string
	sync.RWMutex
}

var roleInheriance = map[string][]string{
	"superuser": {"admin"},
	"admin":     {"user", "converter"},
	"converter": {"user"},
}

/**
expandToInheritors: Expands a role into it's parents/inheritor.
e.g. role `admin` inherits roles `user` & `converter`
This would mean `expandToInheritors("admin")` -> ["admin"]. It does follow inheritance downstream.
This would mean `expandToInheritors("converter")` -> ["converter", "admin"]. It also does not include other roles.
This would mean `expandToInheritors("user")` -> ["user", "admin"]. It just walks up the inheritance recursively
*/
func expandToInheritors(rolesToExpand ...string) []string {
	roles := rolesToExpand
	for _, roleToExpand := range rolesToExpand {
		for parentRole, childRoles := range roleInheriance {
			for _, childRole := range childRoles {
				if childRole == roleToExpand {
					for _, p := range expandToInheritors(parentRole) {
						if !contains(roles, p) {
							roles = append(roles, p)
						}
					}
				}
			}
		}
	}
	return roles
}

var conf = &oauth2.Config{
	ClientID:    "",
	Scopes:      []string{"read_user"},
	Endpoint:    gitlab.Endpoint,
	RedirectURL: "https://srb.t4c.dev/api/v1/auth/callback",
}

func New() Auth {
	defer timetrack.TimeTrack(time.Now())
	a := Auth{}
	a.routeACL = map[string][]string{}

	conf.ClientID = config.GetConfig().Oauth.GitLabAppID
	key = []byte(config.GetConfig().JWTSecret)
	sKey = jose.SigningKey{Algorithm: jose.HS512, Key: key}
	var err error
	sig, err = jose.NewSigner(sKey, (&jose.SignerOptions{}).WithType("JWT"))
	if err != nil {
		panic(err)
	}

	return a
}

/**
AddACL MUST NOT be called dynamically. This is only to add ACLs upon initialization.
*/
func (a *Auth) AddACL(routeName string, roles ...string) {
	defer timetrack.TimeTrack(time.Now())
	if routeName == "" || len(roles) == 0 {
		log.Debugln("Ignoring AddACL call, as no ACL was specified")
		return
	}

	a.RWMutex.Lock()
	defer a.RWMutex.Unlock()

	extendedRoles := expandToInheritors(roles...)

	log.WithField("acl", routeName).Debugf("expanded %+v to %+v", roles, extendedRoles)

	a.routeACL[routeName] = extendedRoles
}

/**
DropACL MUST NOT be called dynamically. This is only to drop ACLs upon initialization.
*/
func (a *Auth) DropACL(routeName string) {
	defer timetrack.TimeTrack(time.Now())
	a.RWMutex.Lock()
	defer a.RWMutex.Unlock()
	delete(a.routeACL, routeName)
}

var ErrForbidden = errors.New("forbidden")

func (a *Auth) Login(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Cache-Control", "no-store")
	var newToken string
	var err error
	if token := request.Header.Get("Authorization"); token != "" {
		newToken, err = a.ExchangeToken(strings.Replace(token, "Bearer ", "", 1))
	} else {
		a.LoginByOauth(writer, request)
		return
	}
	if err == ErrForbidden {
		writer.WriteHeader(http.StatusForbidden)
		return
	}
	if err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}

	writer.Header().Set("Authorization", fmt.Sprintf("Bearer %s", newToken))
	cookie := &http.Cookie{
		Name:     "srb_auth",
		Value:    newToken,
		Path:     "/api",
		Secure:   request.URL.Scheme == "https",
		HttpOnly: true,
		SameSite: http.SameSiteStrictMode,
	}

	http.SetCookie(writer, cookie)
}

func (a *Auth) ExchangeToken(replace string) (string, error) {
	// TODO: Check token against SRB-central auth
	// TODO: Check and renew token if local
	return "", ErrForbidden
}

type OauthProvider struct {
	Name      string
	Enabled   bool
	IconClass string `json:"IconClass,omitempty"`
	Endpoint  string
}

func (a *Auth) ListProviders(writer http.ResponseWriter, _ *http.Request) {
	writer.Header().Set("x-sonicrainboom-workercache", "ignore")
	providers := make([]OauthProvider, 0)

	providers = append(
		providers,
		OauthProvider{
			Name:      "GitLab",
			Enabled:   true,
			Endpoint:  "gitlab",
			IconClass: "fab fa-gitlab",
		},
		OauthProvider{
			Name:     "Facebook",
			Enabled:  false,
			Endpoint: "facebook",
		},
	)

	j, err := json.Marshal(providers)
	if err != nil {
		log.Error(err)
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}
	writer.WriteHeader(http.StatusOK)
	writer.Write(j)
}

func (a *Auth) LoginByOauth(writer http.ResponseWriter, _ *http.Request) {
	writer.Header().Set("Cache-Control", "no-store")
	writer.Header().Set("x-sonicrainboom-workercache", "ignore")

	//TODO: Detect provider to use from path
	state, err := createState("default", uuid.New().String())
	if err != nil {
		log.Error(err)
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}

	url := conf.AuthCodeURL(state, oauth2.AccessTypeOnline)

	stateCookie := &http.Cookie{Name: "srb_state", Path: "/api/v1/auth", Value: state, HttpOnly: true, SameSite: http.SameSiteLaxMode}

	http.SetCookie(writer, stateCookie)

	writer.Header().Set("Location", url)
	writer.WriteHeader(http.StatusFound)
}

func (a *Auth) LoggedIn(writer http.ResponseWriter, r *http.Request) {
	writer.Header().Set("Cache-Control", "no-store")
	writer.Header().Set("x-sonicrainboom-workercache", "ignore")

	user := a.UserByRequest(r)
	if user == nil {
		writer.WriteHeader(http.StatusForbidden)
		return
	}

	// TODO: Return role & shared grants
	user.Issued = 0

	j, err := json.Marshal(user)
	if err != nil {
		log.Error(err)
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}
	writer.WriteHeader(http.StatusOK)
	writer.Write(j)
}

func (a *Auth) PlayerAuthByTokenUser(user *TokenUser) *TokenUser {
	// If we have a restricted token, make sure we don't escalate privileges
	if len(user.RestrictedRouteNames) > 0 {
		if !contains(user.RestrictedRouteNames, "streamSong") ||
			!contains(user.RestrictedRouteNames, "ackStream") ||
			!contains(user.RestrictedRouteNames, "playerControl") {
			return nil
		}
	}

	return &TokenUser{
		ID:       user.ID,
		Username: user.Username,
		Expires:  time.Now().Add(10 * time.Minute).Unix(),
		RestrictedRouteNames: []string{
			"streamSong",
			"ackStream",
			"playerControl",
			"messageHub",
		},
	}
}

func (a *Auth) GetPlayerAuth(writer http.ResponseWriter, r *http.Request) {
	writer.Header().Set("Cache-Control", "no-store")
	writer.Header().Set("x-sonicrainboom-workercache", "ignore")

	user := a.UserByRequest(r)
	if user == nil {
		writer.WriteHeader(http.StatusForbidden)
		return
	}

	var playertoken = a.PlayerAuthByTokenUser(user)

	if playertoken == nil {
		log.Error("PlayerAuth could not be created")
		writer.WriteHeader(http.StatusForbidden)
		return
	}

	retVal := struct {
		AuthToken string     `json:"auth_token,omitempty"`
		Content   *TokenUser `json:"token_content,omitempty"`
	}{
		AuthToken: playertoken.String(),
		Content:   playertoken,
	}

	j, err := json.Marshal(retVal)
	if err != nil {
		log.Error(err)
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}
	writer.WriteHeader(http.StatusOK)
	_, _ = writer.Write(j)
}

var key []byte
var sig jose.Signer
var sKey jose.SigningKey

type State struct {
	Provider string
	UUID     string
}

func createState(provider, uuid string) (string, error) {
	state := State{
		Provider: provider,
		UUID:     uuid,
	}

	j, err := json.Marshal(state)
	if err != nil {
		return "", err
	}
	raw, err := sig.Sign(j)
	if err != nil {
		return "", err
	}
	return raw.CompactSerialize()
}

type GitLabUser struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

type TokenUser struct {
	ID                   int64    `json:"id"`
	Username             string   `json:"user"`
	Issued               int64    `json:"iat,omitempty"`
	Expires              int64    `json:"exp,omitempty"`
	RestrictedRouteNames []string `json:"restricted_routes,omitempty"`
}

func (t TokenUser) String() (token string) {
	j, err := json.Marshal(t)
	if err != nil {
		return ""
	}
	raw, err := sig.Sign(j)
	if err != nil {
		return ""
	}
	token, _ = raw.CompactSerialize()
	return
}

func (a *Auth) Logout(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Cache-Control", "no-store")
	writer.Header().Set("x-sonicrainboom-workercache", "ignore")

	authCookie := &http.Cookie{
		Name:     "srb_auth",
		Path:     "/api",
		Secure:   request.URL.Scheme == "https",
		Expires:  time.Unix(0, 0),
		HttpOnly: true,
		SameSite: http.SameSiteStrictMode,
	}
	writer.Header().Add("Set-Cookie", authCookie.String())
	writer.Header().Set("Location", "/")
	writer.WriteHeader(http.StatusFound)
}

func (a *Auth) Callback(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Cache-Control", "no-store")
	writer.Header().Set("x-sonicrainboom-workercache", "ignore")

	var gitlabUser GitLabUser

	code := request.URL.Query().Get("code")
	state := request.URL.Query().Get("state")
	cookie, err := request.Cookie("srb_state")
	if err != nil {
		log.Error("No Cookie!")
		writer.Header().Set("Location", "/?loginFailed=true&type=cookie")
		writer.WriteHeader(http.StatusFound)
		return
	}
	if cookie.Value != state {
		log.Error("Illegal state!")
		writer.Header().Set("Location", "/?loginFailed=true&type=state")
		writer.WriteHeader(http.StatusFound)
		return
	}

	ctx := context.Background()

	tok, err := conf.Exchange(ctx, code)
	if err != nil {
		writer.Header().Set("Location", "/?loginFailed=true&type=exchange")
		writer.WriteHeader(http.StatusFound)
		return
	}

	client := conf.Client(ctx, tok)
	resp, err := client.Get("https://gitlab.com/api/v4/user")
	if err != nil {
		log.Error(err)
		writer.Header().Set("Location", "/?loginFailed=true&type=api_failed")
		writer.WriteHeader(http.StatusFound)
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		log.Errorf("Response != 200: %d", resp.StatusCode)
		writer.Header().Set("Location", fmt.Sprintf("/?loginFailed=true&type=%d", resp.StatusCode))
		writer.WriteHeader(http.StatusFound)
		return
	}

	data, _ := ioutil.ReadAll(resp.Body)

	err = json.Unmarshal(data, &gitlabUser)
	if err != nil {
		log.Error(err)
		writer.Header().Set("Location", "/?loginFailed=true&type=json_response")
		writer.WriteHeader(http.StatusFound)
		return
	}

	if gitlabUser.ID != 555989 {
		writer.Header().Set("Location", "/?loginFailed=true&type=not_t4cc0re")
		writer.WriteHeader(http.StatusFound)
		return
	}

	//TODO Get from DB
	user := TokenUser{
		ID:       2,
		Username: "t4cc0re",
		Issued:   time.Now().Unix(),
	}

	authToken := user.String()
	if authToken == "" {
		writer.Header().Set("Location", "/?loginFailed=true&type=jwt")
		writer.WriteHeader(http.StatusFound)
		return
	}

	authCookie := &http.Cookie{
		Name:     "srb_auth",
		Value:    authToken,
		Path:     "/api",
		Secure:   request.URL.Scheme == "https",
		HttpOnly: true,
		SameSite: http.SameSiteStrictMode,
	}
	stateCookie := http.Cookie{Name: "srb_state", Path: "/api/v1/auth", Expires: time.Unix(0, 0), HttpOnly: true, SameSite: http.SameSiteLaxMode}
	writer.Header().Add("Set-Cookie", authCookie.String())
	writer.Header().Add("Set-Cookie", stateCookie.String())
	writer.Header().Set("Location", "/")
	writer.WriteHeader(http.StatusFound)
}

func (a *Auth) UserByToken(token string) *TokenUser {
	if token == "" {
		return nil
	}

	tok, err := jwt.ParseSigned(token)
	if err != nil {
		log.Error(err)
		return nil
	}

	var user TokenUser
	err = tok.Claims(key, &user)
	if err != nil {
		log.Error(err)
		return nil
	}

	// Issued later than now (+ time drift)
	if time.Now().Add(30*time.Second).Unix() < user.Issued {
		return nil
	}

	// Expired
	if user.Expires != 0 && time.Now().Add(-30*time.Second).Unix() > user.Expires {
		return nil
	}

	return &user
}

func (a *Auth) UserByRequest(request *http.Request) *TokenUser {
	var token string
	token = request.URL.Query().Get("playerauth")
	if token == "" {
		cookie, err := request.Cookie("srb_auth")
		if err != nil {
			token = strings.Replace(request.Header.Get("Authorization"), "Bearer ", "", 1)
		} else {
			token = cookie.Value
		}
	}

	return a.UserByToken(token)
}
