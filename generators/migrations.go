// +build ignore

package main

import (
	"github.com/go-bindata/go-bindata/v3"
	"os"
	"path"
	"path/filepath"
	"strings"
)

func main() {
	wd, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	if strings.HasSuffix(wd, "/cmd") {
		os.Chdir("../")
	}
	wd, err = os.Getwd()
	if err != nil {
		panic(err)
	}
	c := bindata.NewConfig()
	c.NoMemCopy = true
	c.Output = path.Join(wd, "generated_assets", "migrations", "bindata.go")
	c.HttpFileSystem = false
	c.Prefix = "migrations"
	c.Input = []bindata.InputConfig{
		{
			Path:      filepath.Clean("migrations"),
			Recursive: true,
		},
	}
	c.Package = "migrations"

	err = bindata.Translate(c)
	if err != nil {
		panic(err)
	}
}
