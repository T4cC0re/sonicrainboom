#!/usr/bin/env bash

while [[ -n "$@" ]]; do
    export URL="$1"
    echo '{}' | jq '.url=env.URL' | curl -iXPOST -d @- http://localhost/api/v1/imports
    shift
done

curl -iXPOST http://localhost/api/v1/scan