package transcoders

import (
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"gitlab.com/T4cC0re/silo/v2/sdk"
	"gitlab.com/T4cC0re/silo/v2/server/silo"
	"gitlab.com/T4cC0re/time-track"
	"gitlab.com/sonicrainboom/sonicrainboom/transcoders/types"
	"golang.org/x/crypto/sha3"
	"net/http"
	"os"
	"sort"
	"time"
)

var (
	transcoderCounter = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "transcodes",
		Help: "Total amount of transcodes during process lifetime",
	},
		[]string{
			"targetFormat",
			"transcoder",
			"kind", // "scheduled" or "ondemand"
		},
	)
)

var transcoders = map[string][]types.Transcoder{}
var mimeTypes = map[string]string{}
var extensions = map[string]struct{}{} // struct{} consumes 0 bytes

var cache sdk.SiloSDK
var ready = false

func init() {
	log.Debug("Initializing cache...")
	log.Debug("Initializing cache Done...")
}

func SetSilo(silo sdk.SiloSDK) {
	if ready {
		panic("cannot switch silo SDK once ready")
	}
	cache = silo
}

func Ready() {
	if cache == nil {
		panic("No silo SDK set")
	}
	ready = true
}

func AddTranscoder(name string, targetFormat string, priority uint8, callback types.TranscoderFunc) {
	defer timetrack.TimeTrack(time.Now())
	if _, ok := transcoders[targetFormat]; !ok {
		transcoders[targetFormat] = []types.Transcoder{
			{
				Priority: priority,
				Callback: callback,
				Name:     name,
			},
		}
		return
	}

	transcoders[targetFormat] = append(
		transcoders[targetFormat],
		types.Transcoder{
			Priority: priority,
			Callback: callback,
			Name:     name,
		},
	)

	sort.Slice(transcoders[targetFormat], func(i, j int) bool {
		return transcoders[targetFormat][i].Priority > transcoders[targetFormat][j].Priority
	})
}

func SetMime(targetFormat string, mime string) {
	defer timetrack.TimeTrack(time.Now())
	mimeTypes[targetFormat] = mime
}

func GetMime(targetFormat string) (mime string) {
	defer timetrack.TimeTrack(time.Now())
	var ok bool
	if mime, ok = mimeTypes[targetFormat]; ok {
		return
	}
	return "application/octet-stream"
}

func GetMimes() (mimes map[string]string) {
	defer timetrack.TimeTrack(time.Now())
	mimes = map[string]string{}
	for k, v := range mimeTypes {
		mimes[k] = v
	}
	mimes["default"] = "application/octet-stream"
	return
}

func HasMime(targetFormat string) (ok bool) {
	defer timetrack.TimeTrack(time.Now())
	_, ok = mimeTypes[targetFormat]
	return
}

/**
Just used for scanning whitelist
*/
func AddExtension(extension string) {
	defer timetrack.TimeTrack(time.Now())
	extensions[extension] = struct{}{}
}

func GetExtensions() (ext []string) {
	defer timetrack.TimeTrack(time.Now())
	for extension := range extensions {
		ext = append(ext, extension)
	}
	return
}

func GetFormats() (ext []string) {
	defer timetrack.TimeTrack(time.Now())
	for format := range transcoders {
		ext = append(ext, format)
	}
	return
}

/**
Only point of entry for transcoding requests
Returns:
  local path to transcoded file
*/
func ScheduledTranscode(targetFormat string, uri string) (string, error) {
	defer timetrack.TimeTrackX(time.Now(), "%s | target: %s | %d us", targetFormat)

	var canTranscode bool
	var err error
	var transcoded bool
	var target string

	if _, ok := transcoders[targetFormat]; !ok {
		return "", types.ENoTranscoder
	}

	for _, transcoder := range transcoders[targetFormat] {
		canTranscode, target, err = transcoder.Callback(uri)
		if !canTranscode {
			log.Infof("Could not use transcoder '%s' to transcode '%s' to '%s'. Input not supported.", transcoder.Name, uri, targetFormat)
			continue
		}
		if err != nil {
			log.Errorln(err)
			continue
		}

		transcoderCounter.WithLabelValues(targetFormat, transcoder.Name, "scheduled").Inc()

		transcoded = true
		break
	}
	if !transcoded {
		return "", types.ENoTranscoder
	}
	return target, err
}

func serveRaw(targetFormat string, cacheKey string, response http.ResponseWriter, request *http.Request) (err error) {
	var file *silo.RWASC
	file, err = cache.(*silo.SiloSDKNative).GetRaw(cacheKey)
	if err != nil {
		return
	}
	defer file.Close()
	response.Header().Set("Content-Type", GetMime(targetFormat))
	response.Header().Set("Cache-Control", "public,max-age=3600,proxy-revalidate,no-transform")
	response.Header().Set("ETag", cacheKey)
	http.ServeContent(response, request, "", time.Time{}, file)
	//link := cache.GenerateDownloadLink(cacheKey, 900)
	//http.Redirect(response, request, link, 302)
	return
}

func OndemandTranscode(targetFormat string, uri string, response http.ResponseWriter, request *http.Request) error {
	var canTranscode bool
	var err error       //
	var transcoded bool // Used after the loop to check whether we had a matching transcoder

	log.Infof("OndemandTranscode(%s, %s, w)", targetFormat, uri)

	// TODO: Maybe cache this / do once
	//conf := config.GetConfig()

	cacheKey := fmt.Sprintf(
		"sonicrainboom_%#x.%s",
		sha3.Sum256([]byte(uri)),
		targetFormat,
	)

	has, err := cache.Has(cacheKey)
	if err != nil {
		return err
	}
	if has {
		log.WithField("song", uri).WithField("format", targetFormat).Info("Song in transcoder cache")
		cache.SetTTL(cacheKey, 900)
		response.Header().Set("x-sonicrainboom-transcode", "cached")
		err = serveRaw(targetFormat, cacheKey, response, request)
		return err
	}

	if _, ok := transcoders[targetFormat]; !ok {
		return types.ENoTranscoder
	}

	for _, transcoder := range transcoders[targetFormat] {
		var path string
		canTranscode, path, err = transcoder.Callback(uri)
		if !canTranscode {
			log.Infof("Could not use transcoder '%s' to transcode '%s' to '%s'. Input not supported.", transcoder.Name, uri, targetFormat)
			continue
		}
		if err != nil {
			log.Errorln(err)
			cache.Delete(cacheKey)
			continue
		}

		file, err := os.Open(path)
		if err != nil {
			log.Errorln(err)
			cache.Delete(cacheKey)
			continue
		}
		err = cache.Upload(cacheKey, file)
		if err != nil {
			log.Errorln(err)
			cache.Delete(cacheKey)
			continue
		}

		cache.SetTTL(cacheKey, 900)

		response.Header().Set("x-sonicrainboom-transcode", "ondemand")
		transcoderCounter.WithLabelValues(targetFormat, transcoder.Name, "ondemand").Inc()
		err = serveRaw(targetFormat, cacheKey, response, request)
		if err != nil {
			return err
		}
		transcoded = true
		break
	}

	if !transcoded {
		cache.Delete(cacheKey)
		//TODO: add metrics on this
		return types.ENoTranscoder
	}

	return err
}
