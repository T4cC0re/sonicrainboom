package transcoders

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/sonicrainboom/sonicrainboom/metadata"
	"gitlab.com/sonicrainboom/sonicrainboom/metadata/types"
	"golang.org/x/crypto/sha3"
	"os"
	"os/exec"
	"strings"
)

var ffmpeg string
var ffprobe string

func init() {
	ffmpeg = "ffmpeg"
	ffprobe = "ffprobe"

	AddTranscoder("ffmpeg", "aac", 0, convertAAC256)
	AddTranscoder("ffmpeg", "alac", 0, convertALAC)
	AddTranscoder("ffmpeg", "flac", 0, convertFLAC)
	AddTranscoder("ffmpeg", "wav", 0, convertWAV)
	SetMime("aac", "audio/aac")
	SetMime("aiff", "audio/aiff")
	SetMime("alac", "audio/mp4")
	SetMime("flac", "audio/x-flac")
	SetMime("mp3", "audio/mp3")
	SetMime("wav", "audio/wav")
	AddExtension(".aif")
	AddExtension(".aiff")
	AddExtension(".alac")
	AddExtension(".ape")
	AddExtension(".dsf")
	AddExtension(".flac")
	AddExtension(".m4a")
	AddExtension(".mp3")
	AddExtension(".mpc")
	AddExtension(".oga")
	AddExtension(".ogg")
	AddExtension(".opus")
	AddExtension(".wav")
	AddExtension(".wave")
	AddExtension(".wma")
	metadata.AddMetadataExtractor(RunFfprobe)
}

func convertALAC(uri string) (bool, string, error) {
	out, err := RunFfmpeg(uri, "alac", "ipod", []string{})
	return true, out, err
}

func convertFLAC(uri string) (bool, string, error) {
	out, err := RunFfmpeg(uri, "flac", "flac", []string{})
	return true, out, err
}

func convertAAC256(uri string) (bool, string, error) {
	out, err := RunFfmpeg(uri, "aac", "adts", []string{"-b:a", "256k"})
	return true, out, err
}

func convertWAV(uri string) (bool, string, error) {
	out, err := RunFfmpeg(uri, "pcm_s16le", "wav", []string{"-ar", "44.1k", "-ac", "2"})
	return true, out, err
}

func RunFfmpeg(uri string, codec string, targetFormat string, flags []string) (string, error) {
	cacheFile := fmt.Sprintf(
		"%s/sonicrainboom_%#x.%s.%s",
		os.TempDir(),
		sha3.Sum256([]byte(uri)),
		targetFormat,
		codec,
	)

	args := []string{
		"-i",
		uri,
		"-vn",
		"-c:a",
		codec,
		"-map_metadata",
		"-1",
	}
	args = append(
		args,
		flags...,
	)
	args = append(
		args,
		"-f",
		targetFormat,
		"-y",
		cacheFile,
	)
	log.Infoln(args)

	cmd := exec.Command(ffmpeg, args...)

	cmd.Dir = os.TempDir()
	cmd.Start()

	err := cmd.Wait()

	return cacheFile, err
}

func RunFfprobe(uri string, meta *types.Metadata) {
	args := []string{
		"-i",
		uri,
		"-show_entries",
		strings.Join(
			[]string{
				"stream=" + strings.Join(streamPropertiesToGet, ","),
				"stream_tags=" + strings.Join(tagsToGet, ","),
				"format=" + strings.Join(formatPropertiesToGet, ","),
				"format_tags=" + strings.Join(tagsToGet, ","),
			},
			" : ",
		),
		"-print_format",
		"json",
	}
	cmd := exec.Command(ffprobe, args...)

	cmd.Dir = os.TempDir()

	stdout := new(bytes.Buffer)
	stderr := new(bytes.Buffer)
	cmd.Stdout = stdout
	cmd.Stderr = stderr

	err := cmd.Start()
	if err != nil {
		log.Errorln(err)
	}

	_ = cmd.Wait()

	//log.Infoln(stdout.String())
	//log.Errorln(stderr.String())

	err = json.NewDecoder(stdout).Decode(&meta)
	if err != nil {
		log.Warnln(err)
	}
}

//region ffprobe tag list
var streamPropertiesToGet = []string{
	"index",
	"codec_name",
	"codec_long_name",
	"codec_type",
	"sample_rate",
	"channels",
	"channel_layout",
	"duration",
	"bit_rate",
	"bits_per_sample",
	"bits_per_raw_sample",
}

var formatPropertiesToGet = []string{
	"filename",
	"format_name",
	"format_long_name",
	"duration",
	"size",
	"bit_rate",
}

var tagsToGet = []string{
	"title",
	"artist",
	"album",
	"album_artist",
	"performer",
	"year",
	"comment",
	"copyright",
	"encoder",
	"track",
	"tracktotal",
	"disc",
	"disctotal",
}

//endregion
