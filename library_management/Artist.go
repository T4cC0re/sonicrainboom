package library_management

import (
	"database/sql"
	"errors"
	"gitlab.com/T4cC0re/time-track"
	"time"
)

type Artist struct {
	Id    int
	Title string
}

func (t *LibraryManagement) FetchArtistById(artistId int) (artist *Artist, err error) {
	defer timetrack.TimeTrack(time.Now())

	var a Artist

	err = Library.SQLx.Get(&a, Library.SQLx.Rebind("SELECT id, title FROM artists WHERE id = ?"), artistId)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, ENotFound
		}
		Library.dbErrors.WithLabelValues("artist", "fetch", "query").Inc()
		log.Errorln(err)
		return
	}

	if a.Id == 0 {
		return nil, ENotFound
	}
	artist = &a

	return
}

func (t *LibraryManagement) FetchArtistPaginated(lastId int) (artists []*Artist, maxId int, err error) {
	defer timetrack.TimeTrack(time.Now())
	artists = make([]*Artist, 0) // Prevent returning a nil slice because of JSON marshalling

	err = Library.SQLx.Select(&artists, Library.SQLx.Rebind("SELECT id as Id, title as Title FROM artists WHERE id > ? ORDER BY id ASC LIMIT ?"), lastId, PAGINATION_LIMIT)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, 0, ENotFound
		}
		Library.dbErrors.WithLabelValues("artist", "page", "query").Inc()
		log.Errorln(err)
		return
	}

	if len(artists) > 0 {
		maxId = artists[len(artists)-1].Id
	} else {
		maxId = lastId
	}
	return
}

func (t *LibraryManagement) FetchArtistByTitle(title string) (artist *Artist, err error) {
	defer timetrack.TimeTrack(time.Now())

	var a Artist

	err = Library.SQLx.Get(&a, Library.SQLx.Rebind("SELECT id, title FROM artists WHERE title = ?"), title)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, ENotFound
		}
		Library.dbErrors.WithLabelValues("artist", "by_title", "query").Inc()
		log.Errorln(err)
		return
	}
	if a.Id == 0 {
		return nil, ENotFound
	}
	artist = &a
	log.Warnf("artist: %x", artist)
	return
}

func (t *LibraryManagement) CreateArtist(title string) (artist *Artist, err error) {
	defer timetrack.TimeTrack(time.Now())
	tx, err := Library.SQLx.Begin()
	if err != nil {
		Library.dbErrors.WithLabelValues("artist", "create", "tx_begin").Inc()
		log.Errorln(err)
		return
	}
	defer tx.Rollback()

	_, err = tx.Exec(Library.SQLx.Rebind("INSERT INTO artists (title) VALUES (?) ON CONFLICT (title) DO NOTHING"), title)
	if err != nil {
		Library.dbErrors.WithLabelValues("artist", "create", "exec").Inc()
		log.Errorln(err)
		return
	}
	err = tx.Commit()
	if err != nil {
		Library.dbErrors.WithLabelValues("artist", "create", "tx_commit").Inc()
		log.Errorln(err)
		return
	}
	return Library.FetchArtistByTitle(title)
}

func (t *LibraryManagement) FetchOrCreateArtist(title string) (artist *Artist, err error) {
	defer timetrack.TimeTrack(time.Now())
	artist, err = Library.FetchArtistByTitle(title)
	if artist == nil || artist.Id == 0 || err != nil {
		log.Infof("ArtistId '%s' does not exist. Creating...", title)
	} else {
		return
	}

	artist, err = Library.CreateArtist(title)
	return
}

func (this *Artist) Rename(title string) (err error) {
	defer timetrack.TimeTrack(time.Now())
	err = errors.New("not implemented")
	return
}

//TODO SQLx
func (this *Artist) Delete() (err error) {
	defer timetrack.TimeTrack(time.Now())
	tx, err := Library.db.Begin()
	if err != nil {
		Library.dbErrors.WithLabelValues("artist", "delete", "tx_begin").Inc()
		log.Errorln(err)
		return
	}
	defer tx.Rollback()
	stmt, err := tx.Prepare("DELETE FROM artists WHERE id = ?")
	if err != nil {
		Library.dbErrors.WithLabelValues("artist", "delete", "tx_prepare").Inc()
		log.Errorln(err)
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec(this.Id)
	if err != nil {
		Library.dbErrors.WithLabelValues("artist", "delete", "exec").Inc()
		log.Errorln(err)
		return
	}
	err = tx.Commit()
	if err != nil {
		Library.dbErrors.WithLabelValues("artist", "delete", "tx_commit").Inc()
		log.Errorln(err)
		return
	}
	return
}

func (this *Artist) MergeWithOtherArtists(artistIds ...int) (err error) {
	defer timetrack.TimeTrack(time.Now())

	for _, artistId := range artistIds {
		if artistId == this.Id {
			log.Warn("not merging the same artists. That is stupid.")
			err = nil
			continue
		}
		var artist *Artist
		var artistAlbums []*Album
		var artistSongs []*Song

		log.Infof("Merging artist %d into %d", artistId, this.Id)

		artist, err = Library.FetchArtistById(artistId)
		if err != nil {
			log.WithField("artistId", artistId).Warn("artist does not exist")
			err = nil
			continue
		}

		artistAlbums, err = Library.FetchAlbumsByArtist(artistId)
		if err != nil {
			return
		}

		for _, album := range artistAlbums {

			log.Infof("Merging album %d into artist %d", album.Id, this.Id)

			err = album.ChangeAlbumArtist(this.Id)
			if err != nil {
				return
			}
		}

		// Songs that are not in a updated album (but a compilation etc.)
		artistSongs, _, err = Library.FetchSongsByArtist(artistId, 0, false)
		for _, song := range artistSongs {
			log.Infof("Merging song %d into artist %d", song.Id, this.Id)
			err = song.ChangeArtist(this.Id)
			if err != nil {
				return
			}
		}

		err = artist.Delete()
		if err != nil {
			return
		}
	}
	return
}
