package library_management

import (
	"database/sql"
	timetrack "gitlab.com/T4cC0re/time-track"
	"gitlab.com/sonicrainboom/sonicrainboom/messagehub"
	"gitlab.com/sonicrainboom/sonicrainboom/metadata"
	"gitlab.com/sonicrainboom/sonicrainboom/metadata/types"
	"gitlab.com/sonicrainboom/sonicrainboom/storage_backends"
	types2 "gitlab.com/sonicrainboom/sonicrainboom/storage_backends/types"
	"runtime"
	"strings"
	"time"
)

const (
	PROBE_DONE          = 1
	PROBE_PENDING       = 2
	PROBE_IN_PROGRESS   = 4
	PROBE_ERRORED       = 8
	PROBE_NOT_SUPPORTED = 16
	PROBE_NOT_EMBEDDED  = 128
)

func init() {
}

func (t *LibraryManagement) probeQueueConsumerRunner(c chan *ProbeQueueEntry) {
	var url string
	var err error
	var perms types2.PermissionSet
	for {
		entry := <-c

		_ = messagehub.Instance.Message(messagehub.SysConn, messagehub.Message{Type: "broadcast", Payload: messagehub.Message{Type: "text", Payload: "probeQueueConsumerRunner found new entry"}})

		url, perms, err = storage_backends.Retrieve(entry.Accessor)
		if err != nil {
			entry.SetErrored(true)
			log.Error(err)
			continue
		}
		err = perms.EnsureImport()
		if err != nil {
			entry.SetErrored(true)
			log.Error(err)
			continue
		}

		meta, err := metadata.ExtractMedatata(url)
		if err == types.ENoExtractor {
			log.Errorln(err)
			continue
		}
		if err == types.ENotSupported {
			log.Errorln(err)
			entry.SetUnsupported(true)
			continue
		}
		if err != nil {
			log.Errorln(err)
			entry.SetErrored(true)
			continue
		}
		if meta == nil {
			entry.SetNotEmbedded(true)
			log.Error(ENoMetadata)
			continue
		}

		err = Library.Add(entry.Accessor, meta)
		if err != nil {
			log.Errorln(err)

			//panic(err)

			entry.SetErrored(true)
			continue
		}

		log.Infof("Added: '%s' by '%s' (Accessor: '%s')", meta.Format.Tags.Title, meta.Format.Tags.Artist, entry.Accessor)

		entry.SetDone(true)
	}
}

func (t *LibraryManagement) probe_queueConsumer() {
	queryFlags := PROBE_PENDING

	c := make(chan *ProbeQueueEntry)

	numConsumers := runtime.NumCPU()/2
	//numConsumers := 1

	consumers := 0
	for consumers < numConsumers {
		log.Infof("Starting probe queue consumer (%d of %d)...", consumers+1, runtime.NumCPU()/2)

		go Library.probeQueueConsumerRunner(c)
		consumers++
	}

	for {
		entries, _, _ := Library.FetchProbeQueueByToggledFlags(queryFlags, 0)

		for _, entry := range entries {
			entry.SetInProgress(true)
			c <- entry
		}

		if len(entries) == 0 {
			time.Sleep(5 * time.Second)
		}
	}

}

func (t *LibraryManagement) AddToProbeQueue(accessor string) (err error) {
	defer timetrack.TimeTrack(time.Now())
	_, err = Library.SQLx.Exec(Library.SQLx.Rebind("INSERT INTO probe_queue (accessor, status) VALUES (?, ?) ON CONFLICT(accessor) DO NOTHING"), accessor, PROBE_PENDING)
	if err != nil {
		if strings.HasPrefix(err.Error(), "Error 1062") {
			err = nil // Ignore duplicates
		} else {
			Library.dbErrors.WithLabelValues("probe_queue", "create", "query").Inc()
		}
		return
	}
	return
}

type ProbeQueueEntry struct {
	Id       int
	Status   int
	Accessor string
}

/**
FetchProbeQueueByToggledFlags Returns all probe_queue items that match flags. If flags is negative, returns items that do NOT match the flags
*/
func (t *LibraryManagement) FetchProbeQueueByToggledFlags(flags int, lastId int) (entries []*ProbeQueueEntry, maxId int, err error) {
	defer timetrack.TimeTrack(time.Now())
	entries = make([]*ProbeQueueEntry, 0)

	var query string

	if flags >= 0 {
		query = Library.SQLx.Rebind("SELECT id, status, accessor FROM probe_queue WHERE id > ? AND (status & ?) = ? ORDER BY id ASC LIMIT ?")
	} else {
		flags *= -1
		query = Library.SQLx.Rebind("SELECT id, status, accessor FROM probe_queue WHERE id > ? AND (status & ?) != ? ORDER BY id ASC LIMIT ?")
	}

	err = Library.SQLx.Select(&entries, query, lastId, flags, flags, PAGINATION_LIMIT)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil,0, ENotFound
		}
		Library.dbErrors.WithLabelValues("probe_queue", "fetch", "query").Inc()
		log.Errorln(err)
		return
	}

	if len(entries) > 0 {
		maxId = entries[len(entries)-1].Id
	} else {
		maxId = lastId
	}
	return
}

func (this *ProbeQueueEntry) saveFlags() {
	defer timetrack.TimeTrack(time.Now())
	tx, err := Library.SQLx.Begin()
	if err != nil {
		Library.dbErrors.WithLabelValues("probe_queue", "saveFlags", "tx_begin").Inc()
		log.Errorln(err)
		return
	}
	defer tx.Rollback()
	_, err = tx.Exec(Library.SQLx.Rebind("UPDATE probe_queue SET status = ? WHERE id = ?"), this.Status, this.Id)
	if err != nil {
		Library.dbErrors.WithLabelValues("probe_queue", "saveFlags", "exec").Inc()
		log.Errorln(err)
		return
	}
	err = tx.Commit()
	if err != nil {
		Library.dbErrors.WithLabelValues("probe_queue", "saveFlags", "tx_commit").Inc()
		log.Errorln(err)
		return
	}
	return
}

func (this *ProbeQueueEntry) SetDone(done bool) {
	if done {
		this.Status |= PROBE_DONE
		this.Status &= ^(PROBE_IN_PROGRESS)
		this.Status &= ^(PROBE_PENDING)
	} else {
		this.Status &= ^(PROBE_DONE)
	}
	this.saveFlags()
}

func (this *ProbeQueueEntry) SetInProgress(inProgress bool) {
	if inProgress {
		this.Status |= PROBE_IN_PROGRESS
		this.Status &= ^(PROBE_DONE)
		this.Status &= ^(PROBE_PENDING)
	} else {
		this.Status &= ^(PROBE_IN_PROGRESS)
	}
	this.saveFlags()
}

func (this *ProbeQueueEntry) SetErrored(errored bool) {
	if errored {
		this.Status |= PROBE_ERRORED
		this.Status &= ^(PROBE_DONE)
		this.Status &= ^(PROBE_PENDING)
	} else {
		this.Status &= ^(PROBE_ERRORED)
	}
	this.saveFlags()
}

func (this *ProbeQueueEntry) SetUnsupported(unsupported bool) {
	if unsupported {
		this.Status |= PROBE_NOT_SUPPORTED
		this.Status &= ^(PROBE_DONE)
		this.Status &= ^(PROBE_PENDING)
		this.Status &= ^(PROBE_IN_PROGRESS)
	} else {
		this.Status &= ^(PROBE_NOT_SUPPORTED)
	}
	this.saveFlags()
}

func (this *ProbeQueueEntry) SetNotEmbedded(notEmbedded bool) {
	if notEmbedded {
		this.Status |= PROBE_NOT_EMBEDDED
	} else {
		this.Status &= ^(PROBE_NOT_EMBEDDED)
	}
	this.saveFlags()
}
