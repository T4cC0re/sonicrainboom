package library_management

import (
	"database/sql"
	"github.com/jmoiron/sqlx"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"gitlab.com/sonicrainboom/sonicrainboom/storage_backends"
)

type LibraryManagement struct {
	scanQueue      chan string
	db             *sql.DB
	dbErrors       *prometheus.CounterVec
	LibraryBackend storage_backends.Library
	SQLx           *sqlx.DB
}

var Library *LibraryManagement

func init() {

	Library = &LibraryManagement{}

	Library.scanQueue = make(chan string, SCAN_QUEUE_LENGTH)
	Library.dbErrors = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "database_errors",
		Help: "Total amount of database errors during process lifetime",
	},
		[]string{
			"component",
			"action",
			"kind",
		},
	)
	promauto.NewGaugeFunc(prometheus.GaugeOpts{
		Name: "albums",
		Help: "Amount of albums in database",
	},
		func() float64 { return Library.countTable("albums") })

	promauto.NewGaugeFunc(prometheus.GaugeOpts{
		Name: "artists",
		Help: "Amount of artists in database",
	},
		func() float64 { return Library.countTable("artists") })
	promauto.NewGaugeFunc(prometheus.GaugeOpts{
		Name: "playlists",
		Help: "Amount of playlists in database",
	},
		func() float64 { return Library.countTable("playlists") })
	promauto.NewGaugeFunc(prometheus.GaugeOpts{
		Name: "probe_queue_length",
		Help: "Current length of probe queue with unfinished items",
	}, func() float64 {
		return Library.countTable("probe_queue WHERE (status & 1) != 1")
	})
	promauto.NewGaugeFunc(prometheus.GaugeOpts{
		Name: "songs",
		Help: "Amount of songs in database",
	},
		func() float64 { return Library.countTable("songs") })
	promauto.NewGaugeFunc(prometheus.GaugeOpts{
		Name: "song_versions",
		Help: "Amount of song versions (converted formats) in database",
	},
		func() float64 { return Library.countTable("versions") })
}
