package library_management

import (
	"database/sql"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"gitlab.com/T4cC0re/time-track"
	"gitlab.com/sonicrainboom/sonicrainboom/metadata/types"
	"gitlab.com/sonicrainboom/sonicrainboom/storage_backends"
	"gitlab.com/sonicrainboom/sonicrainboom/transcoders"
	"runtime"
	"strings"
	"time"
)

type ScanInput struct {
	Path     string
	Flags    int
	Metadata *types.Metadata
}

var ENotFound = errors.New("entity not found")
var ENoMetadata = errors.New("no metadata was passed when expected")

const SCAN_QUEUE_LENGTH = 256
const PAGINATION_LIMIT = 50

func (t *LibraryManagement) isLossless(codec string) bool {
	if codec == "flac" {
		return true
	}
	return false
}

func (t *LibraryManagement) countTable(table string) (out float64) {
	rows, err := t.db.Query("SELECT count(*) FROM " + table)
	if err != nil {
		t.dbErrors.WithLabelValues("library", "count", "query").Inc()
		log.Errorln(err)
		return
	}
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(&out)
		if err != nil {
			t.dbErrors.WithLabelValues("library", "count", "scan").Inc()
			log.Errorln(err)
			return
		}
	}
	err = rows.Err()
	if err != nil {
		t.dbErrors.WithLabelValues("library", "count", "rows").Inc()
		log.Errorln(err)
		return
	}
	return
}

func (t *LibraryManagement) lazyQuery(query string) {
	rows, err := t.db.Query(query)
	if err != nil {
		return
	}
	defer rows.Close()
}

func (t *LibraryManagement) Run(db * sql.DB) error {
	log.Infoln("Library is connecting...")
	var err error
	t.db = db
	err = t.db.Ping()
	if err != nil {
		return err
	} else {
		log.Infoln("Library connected!")
	}
	t.db.SetMaxOpenConns(8)
	t.db.SetMaxIdleConns(8)
	t.db.SetConnMaxLifetime(5 * time.Minute)

	backend, err := storage_backends.GetBackendByName("lib")
	if err != nil {
		log.Fatalf("Could not get library backend. %s", err.Error())
	}
	t.LibraryBackend = backend.(storage_backends.Library)
	t.SQLx = sqlx.NewDb(t.db, "pgx")
	t.LibraryBackend.SetDB(t.SQLx)

	return err
}

func (t *LibraryManagement) EnsureLibrary(path string) (err error) {
	defer timetrack.TimeTrack(time.Now())
	if path == "" {
		return
	}
	tx, err := t.SQLx.Begin()
	if err != nil {
		t.dbErrors.WithLabelValues("library", "ensure", "tx_begin").Inc()
		log.Errorln(err)
		return
	}
	defer tx.Rollback()
	_, err = tx.Exec(t.SQLx.Rebind( "INSERT INTO libraries (path) VALUES (?) ON CONFLICT (path) DO NOTHING"), path)
	if err != nil {
		t.dbErrors.WithLabelValues("library", "ensure", "exec").Inc()
		log.Errorln(err)
		return
	}
	err = tx.Commit()
	if err != nil {
		t.dbErrors.WithLabelValues("library", "ensure", "tx_commit").Inc()
		log.Errorln(err)
		return
	}
	return
}

func (t *LibraryManagement) scanQueueConsumer() {
	go func() {
		var accessor string
		var err error

		for {
			filePath := <-t.scanQueue

			var extOk bool
			for _, extension := range transcoders.GetExtensions() {
				if strings.HasSuffix(filePath, extension) {
					extOk = true
				}
			}
			if !extOk {
				continue
			}

			if strings.HasPrefix(filePath, "http") {
				accessor = filePath
			} else if strings.HasPrefix(filePath, "file://") {
				pathToSave := strings.Replace(filePath, "file://", "", 1)
				accessor, err = t.LibraryBackend.Save(pathToSave)
				if err != nil {
					log.Error(err)
					continue
				}
			} else {
				accessor = filePath
			}

			err = Library.AddToProbeQueue(accessor)
			if err != nil {
				log.Error(err)
				continue
			}
		}
	}()
}

func (t *LibraryManagement) RunConsumers() {
	consumers := 0
	for consumers < runtime.NumCPU() {
		log.Infof("Starting library insert queue consumer (%d of %d)...", consumers+1, runtime.NumCPU())
		go t.scanQueueConsumer()
		consumers++
	}

	log.Infof("Resetting probe and import queues...")

	t.lazyQuery("UPDATE probe_queue SET status = 2 WHERE status = 4")
	t.lazyQuery("UPDATE probe_queue SET status = 2 WHERE (status & 2) = 2")
	t.lazyQuery("UPDATE import_queue SET status = 2 WHERE status = 4")
	t.lazyQuery("UPDATE import_queue SET status = 2 WHERE status = 8")
	t.lazyQuery("UPDATE import_queue SET status = 2 WHERE status = 16")

	go Library.probe_queueConsumer()
	//go Library.import_queueConsumer() TODO: Reenable
}

func (t *LibraryManagement) Scan() (errors int, lasterror error) {
	//TODO: Get some kind of progress overview
	log.Info("Starting scan...")
	backend, err := storage_backends.GetBackendByName("local")
	//backend, err := storage_backends.GetBackendByName("googledrive")
	if err != nil {
		return 1, err
	}

	blacklist := []string{".config", "/home/t4cc0re/.local"}

	log.Info("Scan started...")

	errs := 0
	for _, folder := range t.LibraryBackend.GetAllLibraries() {
		err = backend.Traverse(folder, blacklist, t.scanQueue)
		if err != nil {
			errs++
		}
	}

	return errs, err
}

func (t *LibraryManagement) Add(accessor string, meta *types.Metadata) (err error) {
	if meta == nil {
		return ENoMetadata
	}

	var (
		album *Album
		artist *Artist
		song *Song
		version *SongVersion
	)

	artist, err = t.FetchOrCreateArtist(meta.Format.Tags.Artist)
	if err != nil {
		log.Error("FetchOrCreateArtist failed")
		return
	}

	album, err = t.FetchOrCreateAlbum(meta.Format.Tags.Album, meta.Format.Tags.AlbumArtist)
	if err != nil {
		log.Error("FetchOrCreateAlbum failed")
		return
	}

	if len(meta.Streams) < 1 {
		err = errors.New("streamMetadata.streams is empty.")
		return
	}

	trackNr := int(meta.Format.Tags.Track)
	discNr := int(meta.Format.Tags.Disc)

	song, err = t.FetchOrCreateSong(meta.Format.Tags.Title, artist.Id, album.Id, trackNr, discNr, int(meta.Streams[0].Duration*1000))
	if err != nil {
		log.Error("FetchOrCreateSong failed")
		return
	}
	log.Infof("%+v", song)

	format := meta.Format.FormatName
	if format == "" {
		format = "source"
	}

	version, err = t.AddSongVersion(song.Id, format, accessor, &(meta.Streams[0]))
	if err != nil {
		log.Error("AddSongVersion failed")
	}
	log.Infof("version: %+v", version)
	return
}
