package library_management

import (
	"database/sql"
	"errors"
	"gitlab.com/T4cC0re/time-track"
	"strings"
	"time"
)

type Song struct {
	Id        int
	Title     string
	ArtistId  int
	AlbumId   int
	TrackNr   int
	DiscNr    int
	Duration  int
	PlayCount int
}

type SongPopulated struct {
	*Song
	Artist   *Artist
	Album    *AlbumPopulated
	Format   string
	Versions []*SongVersion
}

func init() {
}

func (t *LibraryManagement) IncreasePlayCount(songId int) (err error) {
	_, err = t.SQLx.Exec(t.SQLx.Rebind( "UPDATE songs SET playCount = playCount + 1 WHERE id = ?"), songId)
	return
}

func (t *LibraryManagement) FetchSongById(songId int) (song *Song, err error) {
	defer timetrack.TimeTrack(time.Now())

	var s Song

	err = t.SQLx.Get(&s, t.SQLx.Rebind("SELECT id, title, artistId, albumId, trackNr, discNr, duration, playCount FROM songs WHERE id = ?"), songId)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, ENotFound
		}
		Library.dbErrors.WithLabelValues("song", "fetch", "query").Inc()
		log.Errorln(err)
		return
	}
	if s.Id == 0 {
		return nil, ENotFound
	}
	song = &s

	return
}

func (t *LibraryManagement) FetchSongPaginated(lastId int) (songs []*Song, maxId int, err error) {
	defer timetrack.TimeTrack(time.Now())
	songs = make([]*Song, 0)

	err = t.SQLx.Select(&songs, t.SQLx.Rebind( "SELECT id, title, artistId, albumId, trackNr, discNr, duration, playCount FROM songs WHERE id > ? ORDER BY id ASC LIMIT ?"), lastId, PAGINATION_LIMIT)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, 0, ENotFound
		}
		Library.dbErrors.WithLabelValues("song", "fetch", "query").Inc()
		log.Errorln(err)
		return
	}

	if len(songs) > 0 {
		maxId = songs[len(songs)-1].Id
	} else {
		maxId = lastId
	}

	return
}
func (t *LibraryManagement) FetchSongsByArtist(artistId int, lastId int, paginated bool) (songs []*Song, maxId int, err error) {
	defer timetrack.TimeTrack(time.Now())
	songs = make([]*Song, 0)

	if paginated {
		err = t.SQLx.Select(&songs, t.SQLx.Rebind("SELECT id, title, artistId, albumId, trackNr, discNr, duration, playCount FROM songs WHERE artistId = ? AND id > ? ORDER BY id ASC LIMIT ?"), artistId, lastId, PAGINATION_LIMIT)
	} else {
		err = t.SQLx.Select(&songs, t.SQLx.Rebind("SELECT id, title, artistId, albumId, trackNr, discNr, duration, playCount FROM songs WHERE artistId = ? AND id > ? ORDER BY id ASC"), artistId, lastId)
	}
	if err != nil {
		if err == sql.ErrNoRows {
			return nil,0, ENotFound
		}
		Library.dbErrors.WithLabelValues("song", "by_artist", "query").Inc()
		log.Errorln(err)
		return
	}

	if len(songs) > 0 {
		maxId = songs[len(songs)-1].Id
	} else {
		maxId = lastId
	}

	return
}

func (t *LibraryManagement) FetchSongsByAlbum(albumId int) (songs []*Song, err error) {
	defer timetrack.TimeTrack(time.Now())
	songs = make([]*Song, 0)

	err = t.SQLx.Select(&songs, t.SQLx.Rebind("SELECT id, title, artistId, albumId, trackNr, discNr, duration, playCount FROM songs WHERE albumId = ?"), albumId)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, ENotFound
		}
		Library.dbErrors.WithLabelValues("song", "album", "query").Inc()
		log.Errorln(err)
		return
	}

	return
}

func (t *LibraryManagement) FindSong(title string, artistId int, albumId int, trackNr int, discNr int, duration int) (song *Song, err error) {
	defer timetrack.TimeTrack(time.Now())

	var s Song

	err = t.SQLx.Get(&s, t.SQLx.Rebind("SELECT id, playCount FROM songs WHERE title = ? AND artistId = ? AND albumId = ? AND trackNr = ? AND discNr = ? AND duration = ?"), title, artistId, albumId, trackNr, discNr, duration)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, ENotFound
		}
		Library.dbErrors.WithLabelValues("song", "find", "query").Inc()
		log.Errorln(err)
		return
	}

	if s.Id == 0 {
		return nil, ENotFound
	}
	song = &s
	return
}

func (t *LibraryManagement) CreateSong(title string, artistId int, albumId int, trackNr int, discNr int, duration int) (song *Song, err error) {
	defer timetrack.TimeTrack(time.Now())

	log.Infof("ADD SONG %s", title)

	tx, err := t.SQLx.Begin()
	if err != nil {
		Library.dbErrors.WithLabelValues("song", "create", "tx_begin").Inc()
		log.Errorln(err)
		return
	}
	defer tx.Rollback()
	_, err = tx.Exec(t.SQLx.Rebind("INSERT INTO songs (title, artistId, albumId, trackNr, discNr, duration) VALUES (?, ?, ? ,?, ? ,?) ON CONFLICT (title, artistId, albumId, trackNr, discNr, duration) DO NOTHING"),title, artistId, albumId, trackNr, discNr, duration)
	if err != nil {
		Library.dbErrors.WithLabelValues("song", "create", "exec").Inc()
		log.Errorln(err)
		return
	}
	err = tx.Commit()
	if err != nil {
		Library.dbErrors.WithLabelValues("song", "create", "tx_commit").Inc()
		log.Errorln(err)
		return
	}
	return t.FindSong(title, artistId, albumId, trackNr, discNr, duration)
}

func (t *LibraryManagement) FetchOrCreateSong(title string, artistId int, albumId int, trackNr int, discNr int, duration int) (artist *Song, err error) {
	defer timetrack.TimeTrack(time.Now())
	artist, err = Library.FindSong(title, artistId, albumId, trackNr, discNr, duration)
	if artist == nil || artist.Id == 0 || err != nil {
		log.Infof("Song '%s' does not exist. Creating...", title)
	} else {
		return
	}

	artist, err = Library.CreateSong(title, artistId, albumId, trackNr, discNr, duration)
	return
}

func (this *Song) Rename(name string) (err error) {
	defer timetrack.TimeTrack(time.Now())
	err = errors.New("not implemented")
	return
}

func (this *Song) Delete() (err error) {
	defer timetrack.TimeTrack(time.Now())
	tx, err := Library.db.Begin()
	if err != nil {
		Library.dbErrors.WithLabelValues("song", "delete", "tx_begin").Inc()
		log.Errorln(err)
		return
	}
	defer tx.Rollback()
	stmt, err := tx.Prepare("DELETE FROM songs WHERE id = ?")
	if err != nil {
		Library.dbErrors.WithLabelValues("song", "delete", "tx_prepare").Inc()
		log.Errorln(err)
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec(this.Id)
	if err != nil {
		Library.dbErrors.WithLabelValues("song", "delete", "exec").Inc()
		log.Errorln(err)
		return
	}
	err = tx.Commit()
	if err != nil {
		Library.dbErrors.WithLabelValues("song", "delete", "tx_commit").Inc()
		log.Errorln(err)
		return
	}
	return
}

func (this *Song) ChangeArtist(newArtistId int) (err error) {
	defer timetrack.TimeTrack(time.Now())

	var artist *Artist

	artist, err = Library.FetchArtistById(newArtistId)
	if err != nil {
		return err
	}

	return this.editMergingSongsSQL(artist.Id, 0, "")
}

func (this *SongPopulated) ChangeAlbum(newAlbumId int) (err error) {
	defer timetrack.TimeTrack(time.Now())
	//TODO: Also change artist if it is the same as the album artist

	if this.AlbumId == newAlbumId {
		return
	}

	var newAlbum *Album
	newAlbum, err = Library.FetchAlbumById(newAlbumId)
	if err != nil {
		return
	}

	if this.Album.AlbumArtistId == this.ArtistId {
		return this.editMergingSongsSQL(newAlbum.AlbumArtistId, newAlbumId, "")
	} else {
		return this.editMergingSongsSQL(0, newAlbumId, "")
	}
}

func (this *Song) ChangeAlbum(newAlbumId int) (err error) {
	defer timetrack.TimeTrack(time.Now())

	var pop *SongPopulated
	pop, err = this.Populate()
	if err != nil {
		return
	}
	return pop.ChangeAlbum(newAlbumId)
}

/*
editMergingSongsSQL will edit a song and merge all emerging duplicates into it. Any zero-value fields will be left untouched
*/
func (this *Song) editMergingSongsSQL(targetArtist int, targetAlbum int, targetTitle string) (err error) {
	defer timetrack.TimeTrack(time.Now())

	if targetArtist == 0 {
		targetArtist = this.ArtistId
	}
	if targetAlbum == 0 {
		targetAlbum = this.AlbumId
	}
	if targetTitle == "" {
		targetTitle = this.Title
	}

	var rows *sql.Rows
	duplicates := make([]interface{}, 0)

	//region fetch duplicates
	rows, err = Library.db.Query(
		"SELECT id FROM songs WHERE albumId IN (?, ?) AND title IN (?, ?) AND artistId IN (?, ?) AND trackNr = ? AND discNr = ? and duration = ?",
		this.AlbumId, targetAlbum,
		this.Title, targetTitle,
		this.ArtistId, targetArtist,
		this.TrackNr,
		this.DiscNr,
		this.Duration,
	)
	if err != nil {
		if err == sql.ErrNoRows {
			return ENotFound
		}
		Library.dbErrors.WithLabelValues("song", "merge_dupe_query", "query").Inc()
		log.Errorln(err)
		return
	}
	defer rows.Close()
	for rows.Next() {
		var id int
		err = rows.Scan(&id)
		if err != nil {
			Library.dbErrors.WithLabelValues("song", "merge_dupe_query", "scan").Inc()
			log.Errorln(err)
			return
		}
		if id == this.Id || id == 0 {
			continue
		}
		duplicates = append(duplicates, id)
	}
	err = rows.Err()
	if err != nil {
		Library.dbErrors.WithLabelValues("song", "merge_dupe_query", "rows").Inc()
		log.Errorln(err)
		return
	}
	//endregion

	tx, err := Library.db.Begin()
	if err != nil {
		Library.dbErrors.WithLabelValues("song", "merge_exec", "tx_begin").Inc()
		log.Errorln(err)
		return
	}
	defer tx.Rollback()

	// Handle duplicates...
	if len(duplicates) > 0 {
		var playlistQuery string
		queryIDs := make([]interface{}, len(duplicates)+1)
		queryIDs[0] = this.Id
		for idx, id := range duplicates {
			queryIDs[idx+1] = id
		}

		// region build playlist query
		playlistQuery = "UPDATE songs_in_playlist SET songId = ? WHERE songID IN ("
		var pQMarks []string
		for range duplicates {
			pQMarks = append(pQMarks, "?")
		}
		playlistQuery += strings.Join(pQMarks, ", ") + ")"
		// endregion

		log.Info(playlistQuery)

		var playlistStatement *sql.Stmt

		// region execute playlist query
		playlistStatement, err = tx.Prepare(playlistQuery)
		if err != nil {
			Library.dbErrors.WithLabelValues("song", "merge_exec_playlist", "tx_prepare").Inc()
			log.Errorln(err)
			return
		}
		defer playlistStatement.Close()
		_, err = playlistStatement.Exec(queryIDs...)
		if err != nil {
			Library.dbErrors.WithLabelValues("song", "merge_exec_playlist", "exec").Inc()
			log.Errorln(err)
			return
		}
		// endregion

		var versionsQuery string

		// region build song versions query
		versionsQuery = "UPDATE versions SET songId = ? WHERE songID IN ("
		var vQMarks []string
		for range duplicates {
			vQMarks = append(vQMarks, "?")
		}
		versionsQuery += strings.Join(vQMarks, ", ") + ")"
		// endregion

		log.Info(versionsQuery)

		var versionsStatement *sql.Stmt
		// region execute song versions query
		versionsStatement, err = tx.Prepare(versionsQuery)
		if err != nil {
			Library.dbErrors.WithLabelValues("song", "merge_exec_playlist", "tx_prepare").Inc()
			log.Errorln(err)
			return
		}
		defer versionsStatement.Close()
		_, err = versionsStatement.Exec(queryIDs...)
		if err != nil {
			Library.dbErrors.WithLabelValues("song", "merge_exec_playlist", "exec").Inc()
			log.Errorln(err)
			return
		}
		// endregion

		var deleteQuery string

		// region build delete query
		deleteQuery = "DELETE FROM songs WHERE id IN ("
		var dQMarks []string
		for range duplicates {
			dQMarks = append(dQMarks, "?")
		}
		deleteQuery += strings.Join(dQMarks, ", ") + ")"
		// endregion

		log.Info(deleteQuery, duplicates)

		var deleteStatement *sql.Stmt
		// region execute delete query
		deleteStatement, err = tx.Prepare(deleteQuery)
		if err != nil {
			Library.dbErrors.WithLabelValues("song", "merge_exec_delete_dupes", "tx_prepare").Inc()
			log.Errorln(err)
			return
		}
		defer deleteStatement.Close()
		_, err = deleteStatement.Exec(duplicates...)
		if err != nil {
			Library.dbErrors.WithLabelValues("song", "merge_exec_delete_dupes", "exec").Inc()
			log.Errorln(err)
			return
		}
		// endregion
	}

	var editStatement *sql.Stmt
	// region Perform actual edit
	editStatement, err = tx.Prepare("UPDATE songs SET title = ?, artistId = ?, albumId = ? WHERE id = ?")
	if err != nil {
		Library.dbErrors.WithLabelValues("song", "merge_exec_edit", "tx_prepare").Inc()
		log.Errorln(err)
		return
	}
	defer editStatement.Close()
	_, err = editStatement.Exec(targetTitle, targetArtist, targetAlbum, this.Id)
	if err != nil {
		Library.dbErrors.WithLabelValues("song", "merge_exec_edit", "exec").Inc()
		log.Errorln(err)
		return
	}
	// endregion

	err = tx.Commit()
	if err != nil {
		Library.dbErrors.WithLabelValues("song", "merge_exec", "tx_commit").Inc()
		log.Errorln(err)
		return
	}
	return
}

func (this *Song) Populate() (song *SongPopulated, err error) {
	//TODO: caching.

	artist, err := Library.FetchArtistById(this.ArtistId)
	if err != nil {
		return nil, err
	}
	album, err := Library.FetchAlbumById(this.AlbumId)
	if err != nil {
		return nil, err
	}
	albumP, err := album.Populate()
	if err != nil {
		return nil, err
	}
	versions, err := Library.FindSongVersionsForSong(this.Id)
	if err != nil && err != ENoVersion {
		return nil, err
	}

	Library.RankVersionsByQuality(versions)
	return &SongPopulated{Song: this, Artist: artist, Album: albumP, Versions: versions, Format: GetFomatByVersions(versions)}, nil
}
