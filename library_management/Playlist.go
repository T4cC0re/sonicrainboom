package library_management

import (
	"database/sql"
	"errors"
	"gitlab.com/T4cC0re/time-track"
	"time"
)

type Playlist struct {
	Id    int
	Title string
	Owner int
}

func init() {
}

func (t *LibraryManagement) FetchPlaylistById(playlistId int) (playlist *Playlist, err error) {
	defer timetrack.TimeTrack(time.Now())
	var title string
	var userId int
	var id int
	var rows *sql.Rows

	rows, err = Library.db.Query("SELECT id, title, userId FROM playlists WHERE id = ?", playlistId)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, ENotFound
		}
		Library.dbErrors.WithLabelValues("playlist", "fetch", "query").Inc()
		log.Errorln(err)
		return
	}
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(&id, &title, &userId)
		if err != nil {
			Library.dbErrors.WithLabelValues("playlist", "fetch", "scan").Inc()
			log.Errorln(err)
			return
		}
	}
	err = rows.Err()
	if err != nil {
		Library.dbErrors.WithLabelValues("playlist", "fetch", "rows").Inc()
		log.Errorln(err)
		return
	}
	if id == 0 {
		return nil, ENotFound
	}
	playlist = &Playlist{Id: id, Title: title, Owner: userId}
	return
}

func (t *LibraryManagement) FetchPlaylistByTitle(title string, userId int) (playlist *Playlist, err error) {
	defer timetrack.TimeTrack(time.Now())
	var id int
	var rows *sql.Rows

	rows, err = Library.db.Query("SELECT id, title FROM playlists WHERE title = ? AND userId = ?", title, userId)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, ENotFound
		}
		Library.dbErrors.WithLabelValues("playlist", "by_title", "query").Inc()
		log.Errorln(err)
		return
	}
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(&id, &title)
		if err != nil {
			Library.dbErrors.WithLabelValues("playlist", "by_title", "scan").Inc()
			log.Errorln(err)
			return
		}
	}
	err = rows.Err()
	if err != nil {
		Library.dbErrors.WithLabelValues("playlist", "by_title", "rows").Inc()
		log.Errorln(err)
		return
	}
	if id == 0 {
		return nil, ENotFound
	}
	playlist = &Playlist{Id: id, Title: title, Owner: userId}
	return
}

func (t *LibraryManagement) CreatePlaylist(title string, userId int) (playlist *Playlist, err error) {
	defer timetrack.TimeTrack(time.Now())
	tx, err := Library.db.Begin()
	if err != nil {
		Library.dbErrors.WithLabelValues("playlist", "create", "tx_begin").Inc()
		log.Errorln(err)
		return
	}
	defer tx.Rollback()
	stmt, err := tx.Prepare("INSERT INTO playlists (title, userId) VALUES (?, ?) ON DUPLICATE KEY UPDATE title = title")
	if err != nil {
		Library.dbErrors.WithLabelValues("playlist", "create", "tx_prepare").Inc()
		log.Errorln(err)
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec(title, userId)
	if err != nil {
		Library.dbErrors.WithLabelValues("playlist", "create", "exec").Inc()
		log.Errorln(err)
		return
	}
	err = tx.Commit()
	if err != nil {
		Library.dbErrors.WithLabelValues("playlist", "create", "tx_commit").Inc()
		log.Errorln(err)
		return
	}
	return Library.FetchPlaylistByTitle(title, userId)
}

func (t *LibraryManagement) FetchOrCreatePlaylist(title string, userId int) (playlist *Playlist, err error) {
	defer timetrack.TimeTrack(time.Now())
	playlist, err = Library.FetchPlaylistByTitle(title, userId)
	if playlist == nil || playlist.Id == 0 || err != nil {
		log.Infof("Playlist '%s' does not exist. Creating...", title)
	} else {
		return
	}

	playlist, err = Library.CreatePlaylist(title, userId)
	return
}

func (this *Playlist) Rename(title string) (err error) {
	defer timetrack.TimeTrack(time.Now())
	err = errors.New("not implemented")
	return
}

func (this *Playlist) Delete() (err error) {
	defer timetrack.TimeTrack(time.Now())
	tx, err := Library.db.Begin()
	if err != nil {
		Library.dbErrors.WithLabelValues("playlist", "delete", "tx_begin").Inc()
		log.Errorln(err)
		return
	}
	defer tx.Rollback()
	stmt, err := tx.Prepare("DELETE FROM playlists WHERE id = ?")
	if err != nil {
		Library.dbErrors.WithLabelValues("playlist", "delete", "db_prepare").Inc()
		log.Errorln(err)
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec(this.Id)
	if err != nil {
		Library.dbErrors.WithLabelValues("playlist", "delete", "exec").Inc()
		log.Errorln(err)
		return
	}
	err = tx.Commit()
	if err != nil {
		Library.dbErrors.WithLabelValues("playlist", "delete", "tx_commit").Inc()
		log.Errorln(err)
		return
	}
	return
}
