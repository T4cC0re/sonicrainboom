package log

import (
	logrus "github.com/sirupsen/logrus"
	"os"
)

var logInstance *logrus.Logger
var hook *LogHook

func init() {
	logInstance = logrus.New()
	logInstance.Out = os.Stderr
	hook = new(LogHook)
	logInstance.AddHook(hook)
	if os.Getenv("SRB_LOG_INIT") != "" {
		logInstance.SetLevel(logrus.TraceLevel)
	} else {
		logInstance.SetLevel(logrus.WarnLevel)
	}
}

func Subsystem(subsystem string) logrus.FieldLogger {
	return logInstance.WithField("subsystem", subsystem)
}

func SetLevel(level string) error {
	llevel, err := logrus.ParseLevel(level)
	if err != nil {
		return err
	}

	logInstance.SetLevel(llevel)
	return nil
}
