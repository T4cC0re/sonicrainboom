package log

import (
	"errors"
	"fmt"
	"github.com/coreos/go-systemd/journal"
	"github.com/sirupsen/logrus"
	timetrack "gitlab.com/T4cC0re/time-track"
	"gitlab.com/sonicrainboom/sonicrainboom/buildinfo"
	"path"
	"runtime"
	"time"
	"unicode"
)

func EnableJournal() error {
	if !journal.Enabled() {
		return errors.New("no journal support")
	}

	hook.Journal = true
	return nil
}

type LogHook struct {
	Journal bool
}

func createJournalField(s string) string {
	n := ""
	for _, v := range s {
		if 'a' <= v && v <= 'z' {
			v = unicode.ToUpper(v)
		} else if ('Z' < v || v < 'A') && ('9' < v || v < '0') {
			v = '_'
		}
		// If (n == "" && v == '_'), then we will skip as this is the beginning with '_'
		if !(n == "" && v == '_') {
			n += string(v)
		}
	}
	return n
}

func (h *LogHook) Levels() []logrus.Level {
	return logrus.AllLevels
}

func (h *LogHook) Fire(entry *logrus.Entry) (err error) {
	err = applySource(entry)
	if err != nil {
		return
	}

	if entry == nil || !h.Journal {
		return
	}

	fields := map[string]string{}

	for field, content := range entry.Data {
		switch field {
		case "function":
			fields["CODE_FUNC"] = fmt.Sprintf("%s", content)
		case "file":
			fields["CODE_FILE"] = fmt.Sprintf("%s", content)
		case "line":
			fields["CODE_LINE"] = fmt.Sprintf("%d", content)
		default:
			fields[createJournalField(field)] = fmt.Sprintf("%+v", content)
		}
	}

	fields["SYSLOG_IDENTIFIER"] = "SonicRainBoom"
	fields["SYSLOG_TIMESTAMP"] = entry.Time.Format(time.RFC3339Nano)

	var prio = journal.PriErr
	switch entry.Level {
	case logrus.DebugLevel, logrus.TraceLevel:
		prio = journal.PriDebug
	case logrus.FatalLevel:
		prio = journal.PriCrit
	case logrus.InfoLevel:
		prio = journal.PriInfo
	case logrus.PanicLevel:
		prio = journal.PriEmerg
	case logrus.WarnLevel:
		prio = journal.PriWarning
	case logrus.ErrorLevel:
	default:
	}

	//fmt.Printf("Fields: %+v\n", fields)
	journal.Send(entry.Message, prio, fields)

	return nil
}

func applySource(entry *logrus.Entry) error {
	defer timetrack.TimeTrack(time.Now())

	pc, file, line, ok := runtime.Caller(8)
	if !ok {
		file = "<???>"
		line = 1
	}
	details := runtime.FuncForPC(pc)
	if details != nil {
		entry.Data["function"] = path.Base(details.Name())
	} else {
		entry.Data["function"] = "<???>"
	}
	entry.Data["file"] = file
	entry.Data["line"] = line
	entry.Data["version"] = buildinfo.Version

	return nil
}
