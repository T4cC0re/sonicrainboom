package main

//go:generate go run ../generators/frontend.go -dir=static
//go:generate go run ../generators/frontend.go -dir=frontend/dist/sonicrainboom
//go:generate go run ../generators/migrations.go

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database"
	migrate_postgres "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/golang-migrate/migrate/v4/source/go_bindata"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/stdlib"
	"github.com/sirupsen/logrus"
	"gitlab.com/T4cC0re/time-track"
	"gitlab.com/sonicrainboom/sonicrainboom/api"
	"gitlab.com/sonicrainboom/sonicrainboom/buildinfo"
	"gitlab.com/sonicrainboom/sonicrainboom/config"
	"gitlab.com/sonicrainboom/sonicrainboom/generated_assets/migrations"
	_ "gitlab.com/sonicrainboom/sonicrainboom/importers"
	"gitlab.com/sonicrainboom/sonicrainboom/library_management"
	srb_log "gitlab.com/sonicrainboom/sonicrainboom/log"
	"gitlab.com/sonicrainboom/sonicrainboom/messagehub"
	_ "gitlab.com/sonicrainboom/sonicrainboom/metadata"
	_ "gitlab.com/sonicrainboom/sonicrainboom/storage_backends"
	_ "gitlab.com/sonicrainboom/sonicrainboom/transcoders"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"syscall"
	"time"
)

var sigChannel chan os.Signal
var log = srb_log.Subsystem("main")

func init() {
	sigChannel = make(chan os.Signal, 2)
	signal.Notify(sigChannel, syscall.SIGTERM, syscall.SIGINT, syscall.SIGQUIT)

	log.Infoln("Starting SonicRainboom...")
	//c, err := pgx.ParseConfig("")
	//c.Logger = dbLogger

}

func main() {
	fmt.Fprintln(os.Stderr, "SonicRainBoom")
	fmt.Fprintf(os.Stderr, "Version: %s\n", buildinfo.Version)
	fmt.Fprintf(os.Stderr, "Go runtime: %s\n", runtime.Version())

	fmt.Printf("%+v\n", config.GetConfig())

	db := waitForDB(config.GetConfig().DBURL)
	setupLogging(db)
	if os.Getenv("SRB_NO_MIGRATE") != "true" {
		migrateDB(db, srb_log.Subsystem("database_migration"))
	}

	if os.Getenv("SRB_MIGRATE_ONLY") == "true" {
		log.Infof("SRB_MIGRATE_ONLY==true. Exiting...")
		db.Close()
		os.Exit(0)
	}

	var err error
	if err != nil {
		log.Fatalln(err)
	}

	err = library_management.Library.Run(db)
	if err != nil {
		log.Fatalln(err)
	}

	c, allSystemsGo, server := api.Run()
	log.Info("api.Run returned")

	go func(allSystemsGo chan struct{}) {
		<-allSystemsGo // Wait for everything to powered up in the API

		library_management.Library.RunConsumers()
	}(allSystemsGo)

	go func(sigChannel chan os.Signal, server *http.Server) {
		for {
			sig := <-sigChannel
			log.Warnf("Received signal %d, shutting down...", sig)
			messagehub.Broadcast(messagehub.Message{
				Type:    "close",
				Payload: nil,
			})

			if server != nil {
				err = server.Shutdown(context.Background())
			}

			os.Exit(0)
		}
	}(sigChannel, server)

	for {
		err := <-c
		if err == nil || err == http.ErrServerClosed /* graceful exit */ {
			continue
		}
		log.WithField("err", err).Fatalln("fatal error during initialization")
	}
}

func waitForDB(dburl string) (db *sql.DB) {
	var err error

	log.Infoln("Checking database availability before migration...")

	for {
		log.Infoln("Connecting...")

		db, err = sql.Open("pgx", dburl)
		if err != nil {
			log.WithField("error", err).Error()
			time.Sleep(5 * time.Second)
			continue
		}
		err = db.Ping()

		if err != nil {
			log.Warnln("Connection failed!")
			if strings.HasSuffix(err.Error(), "bad connection") || strings.HasSuffix(err.Error(), "connection refused") {
				log.Warnln("Database not available. Trying again in 5 sec.")
				time.Sleep(5 * time.Second)
				continue
			}
			log.Fatalln(err)
		}
		log.Infoln("Connected!")
		break
	}

	log.Infoln("Database available!")
	return
}

func setupLogging(db *sql.DB) {
	dbl := srb_log.Subsystem("database_driver")

	var err error
	var pgxConn *pgx.Conn

	pgxConn, err = stdlib.AcquireConn(db)
	if err != nil {
		panic(err)
	}
	pgxConn.Config().Logger = srb_log.DBLogger{FieldLogger: &dbl}

}

func migrateDB(db *sql.DB, log logrus.FieldLogger) {
	defer timetrack.TimeTrack(time.Now())
	log.Infoln("Migrating database...")

	var err error
	var driver database.Driver

	driver, err = migrate_postgres.WithInstance(db, &migrate_postgres.Config{})
	if err != nil {
		log.WithField("error", err).Fatal()
	}

	s := bindata.Resource(migrations.AssetNames(), migrations.Asset)
	d, err := bindata.WithInstance(s)
	defer d.Close()

	m, err := migrate.NewWithInstance("migrations", d, "database", driver)
	if err != nil {
		log.Fatalln(err)
	}

	m.Log = srb_log.DBLogger{FieldLogger: &log}

	err = m.Up()
	if err != nil && err != migrate.ErrNoChange {
		log.Fatalln(err)
	}

	log.Infoln("Migrating database done!")
}
