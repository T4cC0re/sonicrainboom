USE `mysql`;
CREATE USER `srb_app` IDENTIFIED WITH mysql_native_password By 'sonicrainboom_application';
CREATE USER `srb_migrate` IDENTIFIED WITH mysql_native_password By 'sonicrainboom_migration';
CREATE DATABASE `sonicrainboom` CHARACTER SET utf8mb4 DEFAULT COLLATE utf8mb4_unicode_ci;
USE `sonicrainboom`;
GRANT INSERT, UPDATE, DELETE, SELECT, CREATE TEMPORARY TABLES ON sonicrainboom.* TO `srb_app`;
GRANT ALL PRIVILEGES ON sonicrainboom.* TO `srb_migrate`;
FLUSH PRIVILEGES;
