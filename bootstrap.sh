#!/usr/bin/env bash

URL="${URL:-http://127.0.0.1}"

docker-compose down
docker-compose build --pull --parallel
docker-compose up -d
sudo rm -rf ./cache

bootstrap () {
  while ! curl $URL/api; do sleep 5; done

  for dir in "$@"; do
    echo "Adding '${dir}'"
    curl -iXPOST -d "{\"path\": \"${dir}\"}" $URL/api/v1/library
  done
  curl -IXPOST $URL/api/v1/scan
}

bootstrap "$@" &

make run
rc=$?
kill %%

exit $rc
