FROM node:14-alpine as frontend
WORKDIR /frontend
RUN apk add --no-cache python build-base ca-certificates
ENV PATH="${PATH}:/frontend/node_modules/.bin"
ENV IN_DOCKER=1
#ENV NPM_TARGET=build_prod
COPY frontend/ /frontend/
RUN make -C /frontend dist/sonicrainboom
#RUN make -C /frontend/rewrite

FROM golang:1.14-alpine as compile
ENV CGO_ENABLED=0
ENV IN_DOCKER=1
ENV SKIP_FRONTEND=1
COPY . /app
RUN ls -lsa /app
RUN apk add --no-cache build-base ca-certificates git
RUN rm -rf /app/frontend/dist
COPY --from=frontend /frontend/dist /app/frontend/dist
RUN make -C /app sonicrainboom

FROM alpine:latest
RUN apk --no-cache add ca-certificates ffmpeg
COPY --from=compile /app/sonicrainboom /bin/sonicrainboom
COPY ./config.docker.yml /etc/sonicrainboom/config.yml
RUN mkdir -p /var/opt/sonicrainboom
WORKDIR /var/opt/sonicrainboom
EXPOSE 80
EXPOSE 443
EXPOSE 5000
CMD ["/bin/sonicrainboom", "--config=/etc/sonicrainboom/config.yml"]
