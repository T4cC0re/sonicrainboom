module gitlab.com/sonicrainboom/sonicrainboom

go 1.14

// Depended on by go-migrate :/ Not actually used though
replace github.com/lib/pq => github.com/lib/pq v1.8.0

require (
	github.com/cloudflare/tableflip v1.2.0
	github.com/coreos/go-systemd v0.0.0-20191104093116-d3cd4ed1dbcf
	github.com/docker/distribution v2.7.1+incompatible
	github.com/ghodss/yaml v1.0.0
	github.com/go-bindata/go-bindata/v3 v3.1.3 // indirect
	github.com/golang-migrate/migrate/v4 v4.12.2
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/websocket v1.4.2
	github.com/jackc/pgx/v4 v4.8.1
	github.com/jmoiron/sqlx v1.2.0
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.7.1
	github.com/sirupsen/logrus v1.6.0
	gitlab.com/T4cC0re/silo/v2 v2.1.0
	gitlab.com/T4cC0re/time-track v0.0.0-20190130002920-f8a364612b66
	golang.org/x/crypto v0.0.0-20200728195943-123391ffb6de
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	golang.org/x/sys v0.0.0-20200808120158-1030fc2bf1d9
	gopkg.in/square/go-jose.v2 v2.5.1
)
