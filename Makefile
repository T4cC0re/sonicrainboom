GIT_REV=$(shell git describe --dirty=-DEV --always)
GO_MOD_NAME=$(shell grep '^module' go.mod  | cut -d ' ' -f2)
GO_DEPS=$(shell find . -wholename '**/*.go' -o -iname 'go.sum' -o -iname 'go.mod')
STATIC_DEPS=$(shell find static)
FRONTEND_DEPS=$(shell find frontend)
POSTMAN_ENV=t4c.dev
PWD=$(shell pwd)
GO_COMMON_FLAGS = -a -trimpath -ldflags="-w -s -extldflags=-Wl,-z,now,-z,relro"
CGO_ENABLED=1
export CGO_ENABLED

.PHONY: all
all: sonicrainboom

.PHONY: clean
clean:
	$(RM) ./sonicrainboom ./sonicrainboom-dev
	$(RM) -r generated_assets/*
	$(MAKE) -C frontend clean
	go clean -cache

.PHONY: test
test:
	docker run --rm -v $(shell pwd)/integrationtests:/integrationtests -w /integrationtests -t postman/newman run -e $(POSTMAN_ENV).postman_environment.json "SonicRainBoom API.postman_collection.json" -r cli,junit

.PHONY: run
run: sonicrainboom-dev
# 	cd frontend && ng build --watch &
# 	cd frontend && tns preview &
	sudo setcap CAP_NET_BIND_SERVICE=+ep ./$<
	DATABASE_URL="postgresql://srb_app:sonicrainboom_application@localhost/sonicrainboom?sslmode=disable" ./$<

frontend/%: $(FRONTEND_DEPS)
#ifndef IN_DOCKER
# If this runs in docker, we assume it uses the multi-stage build which builds the frontend separately
	make -C frontend
#endif

sonicrainboom: cmd/sonicrainboom.go $(GO_DEPS) $(STATIC_DEPS) Makefile go.sum frontend/dist/sonicrainboom
#ifndef IN_DOCKER
# If this runs in docker, we assume it uses the multi-stage build which builds the frontend separately
	rm -rf generated_assets/*
	DEBUG=0 pkgver=$(GIT_REV) go generate ./$<
#endif
	go build -buildmode=pie $(GO_COMMON_FLAGS) -ldflags='-X "$(GO_MOD_NAME)/buildinfo.Version=$(GIT_REV)"' -o ./$@ ./$<

.PHONY: sonicrainboom-dev
sonicrainboom-dev: cmd/sonicrainboom.go $(GO_DEPS) $(STATIC_DEPS) Makefile frontend/dist/sonicrainboom
	rm -rf generated_assets/*
	DEBUG=1 go generate ./$<
	go build -race -tags debug_static $(GO_COMMON_FLAGS) -ldflags='-X "$(GO_MOD_NAME)/buildinfo.Version=DEV"' -o ./$@ ./$<
