// +build linux darwin freebsd

package api

import (
	"fmt"
	"github.com/cloudflare/tableflip"
	silo_server "gitlab.com/T4cC0re/silo/v2/server/silo"
	timetrack "gitlab.com/T4cC0re/time-track"
	"gitlab.com/sonicrainboom/sonicrainboom/config"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func serve(r http.Handler, silo *silo_server.Silo, initErrors chan error, allSystemsGo chan struct{}) *http.Server {
	defer timetrack.TimeTrack(time.Now())
	tlsEnabled := true
	conf := config.GetConfig()

	if conf.APIPortTLS == 0 || conf.Cert == "" {
		log.Warnln("TLS disabled, porttls is 0 or no cert was given")
		tlsEnabled = false
	} else if conf.Key == "" {
		conf.Key = conf.Cert
	}

	upg, err := tableflip.New(tableflip.Options{})
	initErrors <- err

	go func() {
		sig := make(chan os.Signal, 1)
		signal.Notify(sig, syscall.SIGHUP)
		for range sig {
			if silo != nil {
				// Persist current Silo state (when using builtin Silo) before trying to upgrade
				err := silo.Persist()
				if err != nil {
					log.Errorf("State persistence failed:", err)
					continue
				}
			}
			// Try the actual upgrade
			err = upg.Upgrade()
			if err != nil {
				log.Errorf("Upgrade failed:", err)
				continue
			}

			log.Infof("Upgrade succeeded")
		}
	}()

	// Setup HTTP handler
	server := http.Server{
		Handler: r,
	}

	if conf.APIPort != 0 {
		go func() {
			ln, err := upg.Fds.Listen("tcp", fmt.Sprintf("0.0.0.0:%d", conf.APIPort))
			initErrors <- err
			if err != nil {
				return
			}

			initErrors <- server.Serve(ln)
		}()
	}
	if tlsEnabled {
		go func() {
			ln, err := upg.Fds.Listen("tcp", fmt.Sprintf("0.0.0.0:%d", conf.APIPortTLS))
			initErrors <- err
			if err != nil {
				return
			}

			//TODO: Create callback for certs and inotify watch on cert file for changes
			initErrors <- server.ServeTLS(ln, conf.Cert, conf.Key)
		}()
	}

	// We are now officially in business
	allSystemsGo <- struct{}{}
	return &server
}
