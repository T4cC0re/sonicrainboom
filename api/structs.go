package api

type AddLibrary struct {
	Path string `json:"path"`
}

type ImportURL struct {
	URL string `json:"url"`
}